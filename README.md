快读小说
===============
#### 项目基于ThinkPHP5.1采集源基于笔趣阁
> 写的不太好欢迎加入QQ群互相讨论：811389673 
点击链接加入群聊【小说快速搭建采集群】：https://jq.qq.com/?_wv=1027&k=5FADY8P

> [测试网站地址，点击方位，受限于带宽，首页加载会比较慢，注意当前网站所有内容不做商用](http://tbook.eatandshow.com/index.php/index/main/main)
> 
>[代码地址](https://gitee.com/qobn/zbook.git)
###### 安装说明：
- 1、新建一个数据库：
- 2、导入 zbook.sql文件运行
- 3、运行下列语句（注意这个插入语句插入的是我自己的表，要插入你自己的表请自行修改语句）
----------------
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('1', '玄幻');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('2', '奇幻');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('3', '都市');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('4', '言情');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('5', '武侠');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('6', '仙侠');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('7', '历史');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('8', '军事');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('9', '网游');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('10', '竞技');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('11', '科幻');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('12', '灵异');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('13', '同人');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('14', '漫画');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('15', '修真');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('16', '修仙');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('17', '恐怖');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('18', '全本');
 + INSERT INTO `xx`.`bookkind` (`id`, `book_kind`) VALUES ('19', '其他');

###### 创建bookadmin表
```
CREATE TABLE `bookadmin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


```
###### 创建作者表
```
CREATE TABLE `bookauthor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL COMMENT '作者姓名',
  `author_detail` varchar(255) NOT NULL COMMENT '作者简介',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`) USING HASH COMMENT '给作者姓名添加索引快速查询所有信息'
) ENGINE=MyISAM AUTO_INCREMENT=13982 DEFAULT CHARSET=utf8;


```
###### 创建章节目录表
```
CREATE TABLE `bookchapter` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_chapter_name` varchar(255) NOT NULL COMMENT '章节名',
  `book_chapter_id` varchar(255) NOT NULL COMMENT '原章节id 完整带html',
  `book_name_id` varchar(255) NOT NULL COMMENT '书名id',
  `book_chapter_info_id` int(10) NOT NULL COMMENT '对应章节详细id 如：89000',
  `book_author_id` int(10) unsigned zerofill NOT NULL COMMENT '作者id',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_name_id` (`book_name_id`) USING HASH COMMENT '在章节中给书名id加上索引快速查询出所有章节',
  KEY `book_chapter_id` (`book_chapter_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1890994 DEFAULT CHARSET=utf8;


```
###### 创建章节内容表
```
CREATE TABLE `bookchaptercontent` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_chapter_title` varchar(255) NOT NULL COMMENT '书名章节',
  `book_content` text NOT NULL COMMENT '小说章节对应内容',
  `book_name_id` varchar(255) NOT NULL COMMENT '对应的哪本书的id',
  `book_chapter_id` varchar(50) NOT NULL COMMENT '对应的章节id',
  `book_chapter_info_id` int(10) NOT NULL COMMENT '对应章节详细id 如：89000',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_name_id` (`book_name_id`) USING BTREE COMMENT '给book_name_id加索引快速查询章节内容',
  KEY `book_chapter_id` (`book_chapter_id`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=1794282 DEFAULT CHARSET=utf8;


```
###### 创建小说分类表
```
CREATE TABLE `bookkind` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_kind` varchar(255) NOT NULL COMMENT '小说类别',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;


```
###### 创建小说书名表
```
CREATE TABLE `bookname` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(255) NOT NULL COMMENT '书名',
  `book_id_old` varchar(255) NOT NULL COMMENT '原对应网站书id',
  `book_kind_id` int(3) NOT NULL COMMENT '小说类别',
  `book_author_id` int(2) NOT NULL COMMENT '作者id',
  `book_intro` varchar(255) NOT NULL COMMENT '图书简介',
  `book_img` varchar(255) NOT NULL COMMENT '书封面',
  `book_local_img` varchar(255) NOT NULL COMMENT 'self_img_url',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `book_title` (`book_title`) USING HASH COMMENT '给书名添加索引快速查询'
) ENGINE=MyISAM AUTO_INCREMENT=4889 DEFAULT CHARSET=utf8;


```
![image](https://images2018.cnblogs.com/blog/671945/201807/671945-20180726101930414-338045106.png)
![image](https://images2018.cnblogs.com/blog/671945/201807/671945-20180726101941085-641856934.png)
![image](https://images2018.cnblogs.com/blog/671945/201807/671945-20180726101948366-1192441808.png)
![image](https://images2018.cnblogs.com/blog/671945/201807/671945-20180726105605616-264973152.png)

ThinkPHP5.1对底层架构做了进一步的改进，减少依赖，其主要特性包括：

 + 采用容器统一管理对象
 + 支持Facade
 + 注解路由支持
 + 路由跨域请求支持
 + 配置和路由目录独立
 + 取消系统常量
 + 助手函数增强
 + 类库别名机制
 + 增加条件查询
 + 改进查询机制
 + 配置采用二级
 + 依赖注入完善
 + 中间件支持（V5.1.6+）


> ThinkPHP5的运行环境要求PHP5.6以上。


## 目录结构

初始的目录结构如下：

~~~
www  WEB部署目录（或者子目录）
├─application           应用目录
│  ├─common             公共模块目录（可以更改）
│  ├─module_name        模块目录
│  │  ├─common.php      模块函数文件
│  │  ├─controller      控制器目录
│  │  ├─model           模型目录
│  │  ├─view            视图目录
│  │  └─ ...            更多类库目录
│  │
│  ├─command.php        命令行定义文件
│  ├─common.php         公共函数文件
│  └─tags.php           应用行为扩展定义文件
│
├─config                应用配置目录
│  ├─module_name        模块配置目录
│  │  ├─database.php    数据库配置
│  │  ├─cache           缓存配置
│  │  └─ ...            
│  │
│  ├─app.php            应用配置
│  ├─cache.php          缓存配置
│  ├─cookie.php         Cookie配置
│  ├─database.php       数据库配置
│  ├─log.php            日志配置
│  ├─session.php        Session配置
│  ├─template.php       模板引擎配置
│  └─trace.php          Trace配置
│
├─route                 路由定义目录
│  ├─route.php          路由定义
│  └─...                更多
│
├─public                WEB目录（对外访问目录）
│  ├─index.php          入口文件
│  ├─router.php         快速测试文件
│  └─.htaccess          用于apache的重写
│
├─thinkphp              框架系统目录
│  ├─lang               语言文件目录
│  ├─library            框架类库目录
│  │  ├─think           Think类库包目录
│  │  └─traits          系统Trait目录
│  │
│  ├─tpl                系统模板目录
│  ├─base.php           基础定义文件
│  ├─console.php        控制台入口文件
│  ├─convention.php     框架惯例配置文件
│  ├─helper.php         助手函数文件
│  ├─phpunit.xml        phpunit配置文件
│  └─start.php          框架入口文件
│
├─extend                扩展类库目录
├─runtime               应用的运行时目录（可写，可定制）
├─vendor                第三方类库目录（Composer依赖库）
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
~~~

> 可以使用php自带webserver快速测试
> 切换到根目录后，启动命令：php think run

## 升级指导

原有下面系统类库的命名空间需要调整：

* think\App      => think\facade\App （或者 App ）
* think\Cache    => think\facade\Cache （或者 Cache ）
* think\Config   => think\facade\Config （或者 Config ）
* think\Cookie   => think\facade\Cookie （或者 Cookie ）
* think\Debug    => think\facade\Debug （或者 Debug ）
* think\Hook     => think\facade\Hook （或者 Hook ）
* think\Lang     => think\facade\Lang （或者 Lang ）
* think\Log      => think\facade\Log （或者 Log ）
* think\Request  => think\facade\Request （或者 Request ）
* think\Response => think\facade\Reponse （或者 Reponse ）
* think\Route    => think\facade\Route （或者 Route ）
* think\Session  => think\facade\Session （或者 Session ）
* think\Url      => think\facade\Url （或者 Url ）

原有的配置文件config.php 拆分为app.php cache.php 等独立配置文件 放入config目录。
原有的路由定义文件route.php 移动到route目录

## 命名规范

`ThinkPHP5`遵循PSR-2命名规范和PSR-4自动加载规范，并且注意如下规范：

### 目录和文件

*   目录不强制规范，驼峰和小写+下划线模式均支持；
*   类库、函数文件统一以`.php`为后缀；
*   类的文件名均以命名空间定义，并且命名空间的路径和类库文件所在路径一致；
*   类名和类文件名保持一致，统一采用驼峰法命名（首字母大写）；

### 函数和类、属性命名

*   类的命名采用驼峰法，并且首字母大写，例如 `User`、`UserType`，默认不需要添加后缀，例如`UserController`应该直接命名为`User`；
*   函数的命名使用小写字母和下划线（小写字母开头）的方式，例如 `get_client_ip`；
*   方法的命名使用驼峰法，并且首字母小写，例如 `getUserName`；
*   属性的命名使用驼峰法，并且首字母小写，例如 `tableName`、`instance`；
*   以双下划线“__”打头的函数或方法作为魔法方法，例如 `__call` 和 `__autoload`；

### 常量和配置

*   常量以大写字母和下划线命名，例如 `APP_PATH`和 `THINK_PATH`；
*   配置参数以小写字母和下划线命名，例如 `url_route_on` 和`url_convert`；

### 数据表和字段

*   数据表和字段采用小写加下划线方式命名，并注意字段名不要以下划线开头，例如 `think_user` 表和 `user_name`字段，不建议使用驼峰和中文作为数据表字段命名。

## 参与开发

请参阅 [ThinkPHP5 核心框架包](https://github.com/top-think/framework)。

## 版权信息

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2006-2018 by ThinkPHP (http://thinkphp.cn)

All rights reserved。

ThinkPHP® 商标和著作权所有者为上海顶想信息科技有限公司。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
