/*
Navicat MySQL Data Transfer

Source Server         : 47.98.179.79
Source Server Version : 50560
Source Host           : 47.98.179.79:3306
Source Database       : sql47_98_179_79

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2018-08-08 19:14:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bookauthor
-- ----------------------------
DROP TABLE IF EXISTS `bookauthor`;
CREATE TABLE `bookauthor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL COMMENT '作者姓名',
  `author_detail` varchar(255) NOT NULL COMMENT '作者简介',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`) USING HASH COMMENT '给作者姓名添加索引快速查询所有信息'
) ENGINE=MyISAM AUTO_INCREMENT=4781 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bookchapter
-- ----------------------------
DROP TABLE IF EXISTS `bookchapter`;
CREATE TABLE `bookchapter` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_chapter_name` varchar(255) NOT NULL COMMENT '章节名',
  `book_chapter_id` varchar(255) NOT NULL COMMENT '原章节id',
  `book_name_id` varchar(255) NOT NULL COMMENT '书名id',
  `book_author_id` int(10) unsigned zerofill NOT NULL COMMENT '作者id',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_name_id` (`book_name_id`) USING HASH COMMENT '在章节中给书名id加上索引快速查询出所有章节',
  KEY `book_chapter_id` (`book_chapter_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=405935 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bookchaptercontent
-- ----------------------------
DROP TABLE IF EXISTS `bookchaptercontent`;
CREATE TABLE `bookchaptercontent` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_chapter_title` varchar(255) NOT NULL COMMENT '书名章节',
  `book_content` text NOT NULL COMMENT '小说章节对应内容',
  `book_name_id` varchar(255) NOT NULL COMMENT '对应的哪本书的id',
  `book_chapter_id` varchar(50) NOT NULL COMMENT '对应的章节id',
  `book_chapter_info_id` int(10) NOT NULL,
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_name_id` (`book_name_id`) USING BTREE COMMENT '给book_name_id加索引快速查询章节内容',
  KEY `book_chapter_id` (`book_chapter_id`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=391026 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bookkind
-- ----------------------------
DROP TABLE IF EXISTS `bookkind`;
CREATE TABLE `bookkind` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_kind` varchar(255) NOT NULL COMMENT '小说类别',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bookname
-- ----------------------------
DROP TABLE IF EXISTS `bookname`;
CREATE TABLE `bookname` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(255) NOT NULL COMMENT '书名',
  `book_id_old` varchar(255) NOT NULL COMMENT '原对应网站书id',
  `book_kind_id` int(3) NOT NULL COMMENT '小说类别',
  `book_author_id` int(2) NOT NULL COMMENT '作者id',
  `book_intro` varchar(255) NOT NULL COMMENT '图书简介',
  `book_img` varchar(255) NOT NULL COMMENT '书封面',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `book_title` (`book_title`) USING HASH COMMENT '给书名添加索引快速查询'
) ENGINE=MyISAM AUTO_INCREMENT=1031 DEFAULT CHARSET=utf8;
