<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
define('biqige','https://www.biquge5200.cc/');
define('LOCALURL','http://www.zbook.io');
define('SERVICE','http://47.104.183.52');
define('ISCESHI',true);



// 应用公共文件
function getUername(){
    return session('user_name');
}
function getCurrentTime(){
    return date('Y-m-d h:i:s',time());
}
function getMysqlVersion(){
    return phpversion();
}

function getKindName($kind_id)
{
    switch ($kind_id) {
        case 0:
            return '未知';
            break;
        case 1:
            return '玄幻';
            break;
        case 2:
            return '奇幻';
            break;
        case 3:
            return '都市';
            break;
        case 4:
            return '言情';
            break;
        case 5:
            return '武侠';
            break;
        case 6:
            return '仙侠';
            break;
        case 7:
            return '历史';
            break;
        case 8:
            return '军事';
            break;
        case 9:
            return '网游';
            break;
        case 10:
            return '竞技';
            break;
        case 11:
            return '科幻';
            break;
        case 12:
            return '灵异';
            break;
        case 13:
            return '同人';
            break;
        case 14:
            return '漫画';
            break;
        case 15:
            return '修真';
            break;
        case 16:
            return '修仙';
            break;
    }
}
function getReturnData($data){
    $datames = array();
    if (!empty($data)){
        $code = 0;
        $datames = $data;
        $msg="查询成功";
        if (count($datames)>0){
            $mData = array("code"=>$code,"data"=>$datames,"msg"=>$msg);
        }else{
            $mData = array("code"=>$code,"msg"=>$msg);
        }
    }else{
        $code=1;
        $msg="查询失败";
        $mData = array("code"=>$code,"msg"=>$msg);
    }
    return json($mData);
}

/**
 * 获取服务器地址
 * @return string
 */
function getServerUrl(){
    if (ISCESHI){
        return LOCALURL;
    }else{
        return SERVICE;
    }
}

/**
 * 获取图片地址
 * @return string
 */
function getImgUrl(){
    if (ISCESHI){
        return LOCALURL.DIRECTORY_SEPARATOR.'saveimgs';
    }else{
        return SERVICE.DIRECTORY_SEPARATOR.'saveimgs';
    }
}
/**
 * 获取图片地址
 * @return string
 */
function downBookUrl(){
    if (ISCESHI){
        return LOCALURL.DIRECTORY_SEPARATOR.'savebooks'.DIRECTORY_SEPARATOR;
    }else{
        return SERVICE.DIRECTORY_SEPARATOR.'savebooks'.DIRECTORY_SEPARATOR;
    }
}
