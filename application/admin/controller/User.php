<?php
/**
 * 快读小说
 * 项目基于ThinkPHP5.1采集源基于笔趣阁
 * 写的不太好欢迎加入QQ群互相讨论：811389673
 * 点击链接加入群聊【小说快速搭建采集群】：https://jq.qq.com/?_wv=1027&k=5FADY8P
 * author:有点凉了
 */
namespace app\admin\controller;

use think\Controller;
use think\facade\Request;
use app\index\model\Bookadmin;
use think\facade\Session;
class User extends Controller
{
    //用户登录
    public function login()
    {
        return $this->view->fetch();
    }
    //处理用户登录
    public function doLogin()
    {
        $username = Request::param('username');

        $password = sha1(Request::param('password'));
        $bookadmin = Bookadmin::where('username',$username)->find();
        if ($bookadmin['username']!=$username) {
            $res = ['status'=>0, 'msg'=>'账号错误'];
        } elseif ($bookadmin['password'] != $password) {
            $res = ['status'=>0, 'msg'=>'密码错误'];
        } else {
            $res = ['status'=>1, 'msg'=>'登陆成功'];
            //登录成功,将用户名设置到session会话中
            Session::set('username', $bookadmin['username']);
        }
        return $res;
    }
    //用户退出
    public function logout()
    {
        //删除会话信息
        Session::delete('username');

        //跳转到用户登录界面
        $this->redirect('login');
    }

    public function loginIn(){
        $username = Request::param('username');
    }
}