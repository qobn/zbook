<?php
/**
 * 快读小说
 * 项目基于ThinkPHP5.1采集源基于笔趣阁
 * 写的不太好欢迎加入QQ群互相讨论：811389673
 * 点击链接加入群聊【小说快速搭建采集群】：https://jq.qq.com/?_wv=1027&k=5FADY8P
 * author:有点凉了
 */
namespace  app\admin\controller;
use app\common\controller\Base;
use app\index\model\Bookcontent;
use QL\QueryList;
use QL\Ext\CurlMulti;
use app\index\model\BookChapter;
use app\index\model\Bookname;
use app\index\model\Bookkind;
use app\index\model\Bookauthor;
use think\Db;
use think\facade\Request;
class Admin extends Base
{
     public function index(){

         return $this->view->fetch();
     }

    public function welcome(){
        return $this->fetch();
    }
    //用来查询系统变量
    public function server()
    {
        dump($_SERVER);
    }



    public function makeids()
    {
        $rules = [
            //采集a标签的href属性
            'book_author' => ['#info>p:first', 'text'],
        ];

        $index = Request::param('index');
        $left = Request::param('left');
        $right = Request::param('right');
        set_time_limit(3600);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $urlmake = [];
//        $ql = QueryList::getInstance();
        for ($i = $index; $i <= $index; $i++) {
            for ($j = $i . $left; $j <= $i . $right; $j++) {
//                echo $i . '_' . $j;
//                echo "<br>";
                if (ISCESHI) {
                    $url = LOCALURL . "/admin.php/admin/getbiqugelists/id/" . $i . '_' . $j;
                } else {
                    $url = SERVICE . "/admin.php/admin/getbiqugelists/id/" . $i . '_' . $j;
                }
                $urlmake [] = $url;
            }
        }
        $ql = QueryList::getInstance();
        $ql->use(CurlMulti::class);
        $ql->rules($rules)
            ->curlMulti($urlmake)
            ->success(function (QueryList $ql, CurlMulti $curl, $r) {
//                echo "Current url:{$r['info']['url']} \r\n";
                $data = $ql->encoding('UTF-8', 'GB2312')
                    ->removeHead()
                    ->query()->getData();
                $result = $data->all();
                dump($result);
                if (!empty($result)) {
//                    dump('准备执行getbiqugelists');
                    $this->getbiqugelists($r['info']['url']);
                }
                unset($urlmake);
                $ql->destruct();
//
            })->error(function ($errorInfo, CurlMulti $curl) {
//                echo "Current url:{$errorInfo['info']['url']} \r\n";
//                print_r($errorInfo['error']);
            })->start([
                // 最大并发数，这个值可以运行中动态改变。
                'maxThread' => 10,
                // 触发curl错误或用户错误之前最大重试次数，超过次数$error指定的回调会被调用。
//                            'maxTry' => 3,
                // 全局CURLOPT_*
                'opt' => [
                    CURLOPT_TIMEOUT => 3600,
                    //                    CURLOPT_CONNECTTIMEOUT => 1,
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_RETURNTRANSFER => true
                ],
                // 缓存选项很容易被理解，缓存使用url来识别。如果使用缓存类库不会访问网络而是直接返回缓存。
                //'cache' => ['enable' => false, 'compress' => false, 'dir' => null, 'expire' => 86400, 'verifyPost' => false]
            ]);
    }



    /**
     * http://www.biquge.com.tw
     * 笔趣阁采集
     */
    public function getbiqugelists($urlMake = null)
    {
        set_time_limit(3600);   //执行时间无限
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '-1');
        $id = input('param.id');//接收id用于半自动采集
        $idss = [
            $id
        ];
        //采集规则
        $rules = [
            //采集a标签的href属性
            'book_kind' => ['.con_top', 'text'],
            'book_img' => ['#fmimg>img', 'src'],
            'book_name' => ['#info>h1', 'text'],
            'book_author' => ['#info>p:first', 'text'],
            'book_last_update_time' => ['#info>p:eq(2)', 'text'],
            'book_last_update_chapter' => ['#info>p:eq(3)', 'text'],
            'book_intro' => ['#intro>p', 'text'],
            'book_content' => ['.box_con>#list>dl>dd', 'html'],
            'book_content_href' => ['.box_con>#list>dl>dd>a', 'href'],
            'book_content_name' => ['.box_con>#list>dl>dd', 'text']
        ];
        //全自动采集
        if (empty($urlMake)) {
            foreach ($idss as $id) {
                $urllist [] = 'http://www.biquge.com.tw/' . $id . '/';
            }
        } else {
            $urllist[] = $urlMake;
        }
        $ql = QueryList::getInstance();
        $ql->use(CurlMulti::class);
        $ql->rules($rules)
            ->curlMulti($urllist)
            ->success(function (QueryList $ql, CurlMulti $curl, $r) {
//                echo "getbiqugelists Current url:{$r['info']['url']} \r\n";
                $data = $ql->encoding('UTF-8', 'GB2312')
                    ->removeHead()
                    ->query()->getData();
                $result = $data->all();
                if (!empty($result)) {
                    $this->bookname [] = $result[0];
                    foreach ($result as $key => $value) {
                        $every = [];
                        $lastvalue =  $value['book_content'];
                        $index = stripos($lastvalue, "/");
                        $last = stripos($lastvalue, "html");
                        $id = substr($lastvalue, $index, $last + 4 - $index);
                        $every['book_chapter_id'] = $id;
                        $every['book_chapter_name'] = $value['book_content_name'];
                        $this->chapter[] = $every;
                        $this->urlLists[] = "http://www.biquge.com.tw" . $id;
                        $this->ids[] = $id;
                    }
                    $bookkinds = Bookkind::all();
                    $bookkinds = $bookkinds->toArray();
                    $booknamearray = $this->bookname;
                    $book_kindnametitle = [];
                    foreach ($booknamearray as $key => $value) {
                        $book_kindnametitle[] = $value['book_kind'];
                        $lastvalue = $value['book_content_href'];
                        $lastvalue = explode('/',$lastvalue);
                        $booknamearray[$key]['book_id_old'] = $lastvalue[1];
                    }
                    foreach ($bookkinds as $value) {

                        foreach ($book_kindnametitle as $key => $val) {
                            $index = stripos($book_kindnametitle[$key],$value['book_kind'],1);
                            if ($index>0){
                                $booknamearray[$key]['book_kind'] = $value['id'];
                                break(2);
                            }else{
                                $booknamearray[$key]['book_kind'] = 19;
                            }
                        }
                    }
                    $insertAuthorIds = [];
                    $insertAuthorIds = $this->setAuthor($booknamearray);
                    $book_old_ids = [];
                    $book_old_id = '';
                    if (!empty($this->ids)) {
                        $book_old_id = $this->ids[0];
                        $book_old_ids = explode('/', $book_old_id);
                        $book_old_id = $book_old_ids[1];
                    }
                    unset($book_old_ids);
                    $insertBookIds = [];
                    $insertBookIds = $this->setBookname($booknamearray, $insertAuthorIds, $bookname_push = [], $book_old_id);
                    if (!empty($insertBookIds)) {
                        $this->insertBookIdSelf = $insertBookIds[0];
                    } else {
                        $this->insertBookIdSelf = 0;
                    }
                    $this->getDataFromSqlCompare($booknamearray,$insertBookIds, $insertAuthorIds);

                    $this->getbiqugebookinfo($this->urlLists);
                    unset($insertBookIds);
                    unset($insertAuthorIds);
                    unset($booknamearray);
                    unset($this->ids);
                    unset($urllist);
                    unset($this->chapter);
                }
                $ql->destruct();
            })->error(function ($errorInfo, CurlMulti $curl) {
//                echo "Current url:{$errorInfo['info']['url']} \r\n";
//                print_r($errorInfo['error']);
            })->start([
                //                // 最大并发数，这个值可以运行中动态改变。
                'maxThread' => 10,
                //                // 触发curl错误或用户错误之前最大重试次数，超过次数$error指定的回调会被调用。
                'maxTry' => 3,
                // 全局CURLOPT_*
                'opt' => [
                    CURLOPT_TIMEOUT => 3600,
                    //                    CURLOPT_CONNECTTIMEOUT => 1,
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_RETURNTRANSFER => true,
                ],
                // 缓存选项很容易被理解，缓存使用url来识别。如果使用缓存类库不会访问网络而是直接返回缓存。
                //                    'cache' => ['enable' => false, 'compress' => false, 'dir' => null, 'expire' => 86400, 'verifyPost' => false]
            ]);
//        return (json_encode(array('code'=>0,'url'=>$this->urlLists)));
    }



    public function getbiqugebookinfo($urls = [], $isself = true)
    {
//        dump('执行getbiqugebookinfo');
        if (count($urls) <= 0) {
            return;
        }
        set_time_limit(3600);      //执行时间无限
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '-1');
        $ruleinfo = [
            'book_chapter_title' => ['.bookname>h1', 'text'],
            'book_content' => ['div#content', 'text']
        ];
        $ql = QueryList::getInstance();
        $ql->use(CurlMulti::class);
        $ql->rules($ruleinfo)
            ->curlMulti($urls)
            ->success(function (QueryList $ql, CurlMulti $curl, $r) {
//                echo "Current url:{$r['info']['url']} \r\n" . "<br>";
                $url = $r['info']['url'];
                $url = explode('/', $url);
                $book_chapter_id = '/' . $url[3] . '/' . $url[4];
                $book_name_id = $url[3];
//                dump($book_name_id);
                $data = $ql->encoding('UTF-8', 'GB2312')
                    ->removeHead()
                    ->query()->getData();
                $result = $data->all();
                if (!empty($result)) {
                    foreach ($result as $key => $value) {
                        $result[$key]['book_name_id'] = $book_name_id;
                        $result[$key]['book_chapter_id'] = $book_chapter_id;
                    }
                    $bookcontents = Bookcontent::where('book_name_id', $book_name_id)
                        ->field(['book_chapter_title', 'book_chapter_id'])
                        ->select();
                    $result = $this->setValueToKey($result, 'book_chapter_title', 'book_chapter_id');
                    $bookcontents = $this->setValueToKey($bookcontents, 'book_chapter_title', 'book_chapter_id');
                    foreach ($result as $key => $value) {
                        foreach ($bookcontents as $keyval => $val) {
                            if ($key == $keyval) {
                                unset($result[$key]);
                            }
                        }
                    }
                    $bookcontent = new Bookcontent();
                    $bookcontent->saveAll($result);
                }
                unset($result);
                unset($bookcontents);
//                unset($this->urlLists);
                unset($urls);
                // 释放资源
                $ql->destruct();
                $urlnew [] = $r['info']['url'];
//                exit(json_encode(array('code'=>0,'url'=>$r['info']['url'])));
            })->error(function ($errorInfo, CurlMulti $curl) {
//                echo "Current url:{$errorInfo['info']['url']} \r\n";
//                print_r($errorInfo['error']);
            })->start([
//                // 最大并发数，这个值可以运行中动态改变。
                'maxThread' => 10,
                // 触发curl错误或用户错误之前最大重试次数，超过次数$error指定的回调会被调用。
                'maxTry' => 3,
                // 全局CURLOPT_*
                'opt' => [
                    CURLOPT_TIMEOUT => 3600,
//                    CURLOPT_CONNECTTIMEOUT => 1,
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_RETURNTRANSFER => true,
                ],
                // 缓存选项很容易被理解，缓存使用url来识别。如果使用缓存类库不会访问网络而是直接返回缓存。
                //'cache' => ['enable' => false, 'compress' => false, 'dir' => null, 'expire' => 86400, 'verifyPost' => false]
            ]);
    }

    /**
     * 根据数据库已经存储章节更新章节内容
     * @throws \think\exception\DbException
     */
    public function getBookContent()
    {
        set_time_limit(3600);
        $result = Db::query('SELECT DISTINCT bookchapter.book_chapter_id FROM bookchapter WHERE bookchapter.book_chapter_id not in(SELECT book_chapter_id FROM bookchaptercontent);');
//        dump($result);
        $urls = [];
        foreach ($result as $res) {
            $url = biqige . ($res['book_chapter_id']);
            $urls [] = $url;
        }
        $this->getbiqugebookinfo($urls, false);
        //        $booknames = Bookname::all();
        ////        $booknames = $booknames->toArray();
        //        Bookname::chunk(2,function($booknames)	{
        //            foreach($booknames	as	$bookname){
        //                //	处理user模型对象
        //                $book_id = $bookname->id;
        //                $this->insertBookIdSelf = $book_id;
        //                $bookchapters = IndexModel::where('id',$book_id)
        //                    ->field(['book_chapter_id_old'])
        //                    ->select();
        //                $bookchapters = $bookchapters->toArray();
        //                $bookcontents = Bookcontent::where('id',$book_id)
        //                    ->field(['book_chapter_id'])
        //                    ->select();
        //                $bookcontents = $bookcontents->toArray();
        //                foreach ($bookchapters as $key=>$bookchapter){
        //                    foreach ($bookcontents as $bookcontent){
        //                        if ( ($bookchapter['book_chapter_id_old']) == ($bookcontent['book_chapter_id']) ){
        //                            unset($bookchapters[$key]);
        //                        }
        //                    }
        //                }
        //                $urls = [];
        //                foreach ($bookchapters as $bookchapter){
        //                    $url = biqige.($bookchapter['book_chapter_id_old']);
        //                    $urls [] = $url;
        //                }
        //                unset($bookchapters);
        ////                    unset($book_id);
        //                dump($urls);
        //                $this->getbiqugebookinfo($urls);
        //            }
        //        });
    }

    /**
     * 重置索引
     * @param $res
     * @param $left
     * @param $right
     * @return array
     */
    public function setValueToKey($res, $left, $right)
    {
        if (count($res) <= 0) {
            return [];
        }
        $resReturn = [];
        foreach ($res as $key => $value) {
            $resReturn[$value[$left] . $value[$right]] = $value;
        }
        return $resReturn;
    }

    /**
     * 差异化插入章节信息
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getDataFromSqlCompare($booknamearray,$insertBookIds = [], $insertAuthorIds = [])
    {
//        if (count($insertBookIds) <= 0 && count($insertAuthorIds) <= 0) {
//            return;
//        }
        if (empty($booknamearray)){
            return;
        }
        $sqldata = BookChapter::where('book_name_id', $booknamearray[0]['book_id_old'])
            ->field(['book_chapter_id', 'book_chapter_name'])
            ->select();
        $res = $sqldata->toArray();
        $chaptersmearge = $this->chapter;
        foreach ($chaptersmearge as $key => $value) {
            $chaptersmearge[$key]['book_name_id'] = $booknamearray[0]['book_id_old'];
            $chaptersmearge[$key]['book_author_id'] = $insertAuthorIds[0];
        }
        $ressql_set_key_result = $this->setValueToKey($res, 'book_chapter_id', 'book_chapter_name');
        $netsql_set_key_result = $this->setValueToKey($chaptersmearge, 'book_chapter_id', 'book_chapter_name');
        foreach ($ressql_set_key_result as $key => $localval) {
            foreach ($netsql_set_key_result as $keynet => $netval) {
                if ($key == $keynet) {
                    unset($netsql_set_key_result[$keynet]);
                }
            }
        }
        $bookchapter = new BookChapter();
        $bookchapter->saveAll($netsql_set_key_result);
        unset($chaptersmearge);
        unset($netsql_set_key_result);
        unset($ressql_set_key_result);
        unset($res);
        unset($sqldata);
    }

    /**
     * 插入书名信息
     * @param $booknamearray
     * @param $bookname_push
     */
    public function setBookname($booknamearray, $insertAuthorIds, $bookname_push, $book_old_id){
        $bookname_ids = [];
        foreach ($booknamearray as $key => $value) {
            $booknameself = Bookname::where('book_title', $value['book_name'])
                ->field(['book_author_id', 'book_title', 'id'])
                ->find();
            if (!empty($booknameself)) {
                $outside = $insertAuthorIds[$key] . $value['book_name'];
                $inside = $booknameself->book_author_id . $booknameself->book_title;
                if ($outside == $inside) {
                    $bookname_ids [] = $booknameself->id;
                }
            } else {
                $bookname_push['book_title'] = $value['book_name'];
                $bookname_push['book_kind_id'] = $value['book_kind'];
                $bookname_push['book_author_id'] = $insertAuthorIds[$key];
                $bookname_push['book_intro'] = $value['book_intro'];
                $bookname_push['book_img'] = biqige . $value['book_img'];
                $bookname_push['book_id_old'] = $book_old_id;
                $bookname = Bookname::create($bookname_push);
                if ($bookname->id > 0) {
                    $bookname_ids [] = $book_old_id;
                }
            }
        }
        unset($bookname_push);
        return $bookname_ids;
    }


    /**
     * @param $booknamearray
     * @return string
     */

    public function setAuthor($booknamearray){
        $insertAuthorIds = [];
        foreach ($booknamearray as $key => $value) {
            $book_autho = Bookauthor::where('author', $value['book_author'])
                ->field(['id'])
                ->find();
            if (!empty($book_autho) && $book_autho->id > 0) {
                $insertAuthorIds [] = $book_autho->id;
            } else {
                $book_author = explode("：",$value['book_author']);
                $Bookauthor_array = [];
                $Bookauthor_array['author'] = $book_author[1];
                $Bookauthor_array['author_detail'] = $book_author[1];
                $Bookauthor = Bookauthor::create($Bookauthor_array);
                $insertAuthorIds [] = $Bookauthor->id;
            }
        }
        unset($Bookauthor_array);
        return $insertAuthorIds;
    }

    public function setChapterOther(){
        set_time_limit(3600);
        $index = Request::param('admin');
        $size = Request::param('size');
        for ($i = $index;$i<=143588;$i++){//这里在需要的时候<=$size
            sleep(5);
            $bookchapter = BookChapter::get($i);
            if (!empty($bookchapter)){
                if (strpos(($bookchapter->book_name_id),'_')>0){
//                    dump(1);
                    continue;
                }else{
//                    dump(2);
                    $book_chapter_ids = $bookchapter->book_chapter_id;
                    $book_chapter_ids = explode('/',$book_chapter_ids);
                    $book_chapter_id = $book_chapter_ids[1];
                    $bookchapter->book_name_id =  $book_chapter_id;
                    $bookchapter->save();
                    unset($book_chapter_id);
                    unset($book_chapter_ids);
                }
                unset($bookchapter);
            }

        }
    }
}