<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/13 0013
 * Time: 下午 3:34
 */

namespace app\admin\controller;



use app\common\controller\Base;
use app\index\model\Bookcontent;
use app\index\model\Bookname;
use think\facade\Request;

class Booklist extends Base
{
    function booklist(){
        $booknames = Bookname::where('id','>','0')
            ->order('id','desc')
            ->paginate(10);
        $page = $booknames->render();
        $this->view->assign('booknames',$booknames);
        $this->view->assign('page',$page);
        return $this->view->fetch();
    }
    function bookChapter(){//直接查询章节内容
//        Bookcontent::where()
        $book_name_id = Request::param('book_name_id');
        $bookcontents = Bookcontent::where('book_name_id',$book_name_id)
            ->order('book_chapter_info_id','asc')
            ->select();
        $this->view->assign('bookcontents',$bookcontents);
        return $this->view->fetch();
    }
}