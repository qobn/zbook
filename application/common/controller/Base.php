<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/20 0020
 * Time: 下午 1:49
 */
namespace  app\common\controller;
use  think\Controller;

class Base extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->username = session('username');
        // 未登录的用户不允许访问
        if(!session('username')){
//            header('Location: /admin.php/user/login');
            $this->error('请登录~','user/login');
            exit;
        }
    }
}