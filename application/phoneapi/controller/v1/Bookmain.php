<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/21 0021
 * Time: 下午 4:12
 * 这个在控制器下新建目录v1控制版本的  需要用到多级路由将对应的操作路由到对应方法
 * 详情查看route/route.php文件对应的路由信息
 */

namespace app\phoneapi\controller\v1;

use think\Controller;
use app\index\model\Bookname;
use app\index\model\Bookauthor;
use app\index\model\Bookkind;
use think\Db;
use think\facade\Request;
use app\index\model\Bookcontent;
class Bookmain extends Controller
{
    /**
     * 各6本
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function bookmain()
    {
        $xuanhuans = Bookname::where('book_kind_id', 1)
            ->limit(6)
            ->select();
        $xuanhuans = $xuanhuans->toArray();
        $dushus = Bookname::where('book_kind_id', 3)
            ->limit(6)
            ->select();
        $dushus = $dushus->toArray();
        $xiuzhens = Bookname::where('book_kind_id', 15)
            ->limit(6)
            ->select();
        $xiuzhens = $xiuzhens->toArray();
        $lishis = Bookname::where('book_kind_id', 7)
            ->limit(6)
            ->select();
        $lishis = $lishis->toArray();
        $wangyous = Bookname::where('book_kind_id', 9)
            ->limit(6)
            ->select();
        $wangyous = $wangyous->toArray();
        $kehuans = Bookname::where('book_kind_id', 11)
            ->limit(6)
            ->select();
        $kehuans = $kehuans->toArray();
        $result = array_merge($xuanhuans, $dushus, $xiuzhens, $lishis, $wangyous, $kehuans);
        $bookauthors = Bookauthor::all();
        $bookauthors = $bookauthors->toArray();
        $bookkinds = Bookkind::all();
        $bookkinds = $bookkinds->toArray();
        foreach ($result as $key => $res) {
            foreach ($bookauthors as $keyInfo => $bookauthor) {
                if ($res['book_author_id'] == $keyInfo) {
                    if (empty($bookauthor['author'])) {
                        $result[$key]['author'] = "出错了";
                    } else {
                        $result[$key]['author'] = $bookauthor['author'];
                    }

                }
                foreach ($bookkinds as $bookkind) {
                    if ($result[$key]['book_kind_id'] == $bookkind['id']) {
                        $result[$key]['book_kind_name'] = $bookkind['book_kind'];
                    }
                    if ($result[$key]['book_kind_id'] == 0) {
                        $result[$key]['book_kind_name'] = '未知';
                    }
                }
            }
        }
        $xuanhuanResults = [];
        $xiuzhenResults = [];
        $dushiResults = [];
        $lishiResults = [];
        $wangyouResults = [];
        $kehuanResults = [];
        foreach ($result as $key => $value) {
            if ($value['book_kind_id'] == 1) {
                $xuanhuanResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 15) {
                $xiuzhenResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 3) {
                $dushiResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 7) {
                $lishiResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 9) {
                $wangyouResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 11) {
                $kehuanResults [] = $result[$key];
            }
        }

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('id', '>', '0')
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key => $value) {
            $booknamesUpdate_ids [] = $value['book_id_old'];
            foreach ($bookauthors as $keyInfo => $bookauthor) {
                if ($value['book_author_id'] == $bookauthor['id']) {
                    if (empty($bookauthor['author'])) {
                        $booknamesUpdates[$key]['author'] = "出错了";
                    } else {
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }
                }
            }
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        foreach ($booknamesUpdate_ids as $key => $value) {
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql .= " '/{$value}%' ";
            $sql .= " ORDER BY book_chapter_id desc";
            $sql .= " limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)) {
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            } else {
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id = str_replace('/', '-', $bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }
        }
//        dump($booknamesUpdates);
        $booknamesCreates = Bookname::where('id', '>', '0')
            ->order('create_time desc')
            ->limit(25)
            ->select();
        $booknamesCreates = $booknamesCreates->toArray();
        foreach ($booknamesCreates as $key => $value) {
            foreach ($bookauthors as $keyInfo => $bookauthor) {
                if ($value['book_author_id'] == $bookauthor['id']) {
                    if (empty($bookauthor['author'])) {
                        $booknamesCreates[$key]['author'] = "出错了";
                    } else {
                        $booknamesCreates[$key]['author'] = $bookauthor['author'];
                    }
                }
            }
            foreach ($bookkinds as $bookkind) {
                if ($booknamesCreates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesCreates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesCreates[$key]['book_kind_id'] == 0) {
                    $booknamesCreates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $code = 0;
        $data = array();
        $msg = '操作成功';

        if (count($result) > 0) {
            $code = 0;
            $data = $result;
            $msg = "查询成功";
            $mData = array("code" => $code, "data" => $data, "msg" => $msg);
        } else {
            $code = 1;
            $msg = "查询失败";
            $mData = array('code' => $code, 'msg' => $msg);
        }
        return json($mData);
    }

    /**
     * $@param id 接收参数
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function bookList()
    {
        $id = Request::param('id');
        $bookname = Bookname::where('book_id_old', $id)->find();
        if (empty($bookname)) {
            $code = 2;
            $msg = "参数为空";
            return json(array("code" => $code, "msg" => $msg));
        }
        $kind_name = getKindName($bookname->book_kind_id);
        $bookaurhor = Bookauthor::where('id', $bookname->book_author_id)
            ->find();
        $book_aurhor = $bookaurhor->author;
        $sql = "SELECT book_chapter_name,book_chapter_id,book_name_id,book_author_id,book_chapter_info_id  from bookchapter WHERE book_chapter_id like ";
        $sql .= " '/{$bookname->book_id_old}%' ";
        $sql .= " ORDER BY book_chapter_info_id asc";
        $bookchapters = Db::query($sql);
//        $code = 0;
//        $data = array();
//        $msg = '操作成功';
//        if (count($bookchapters) > 0) {
//            $code = 0;
//            $data = $bookchapters;
//            $msg = "查询成功";
//            $mData = array("code" => $code, "data" => $data, "msg" => $msg);
//        } else {
//            $code = 1;
//            $msg = "查询失败";
//            $mData = array("code" => $code, "msg" => $msg);
//        }
        return getReturnData($bookchapters);
    }
    public function bookInfo(){
        $code = 0;
        $data=array();
        $msg="操作成功";
        $book_id_old = Request::param('id');
        $chapterid = Request::param('chapterid');

        $bookname = Bookname::where('book_id_old',$book_id_old)
            ->field(['book_title','book_kind_id','book_id_old'])
            ->find();
        $book_title = $bookname->book_title;
        $kind_name = getKindName($bookname->book_kind_id);
        $where = [
            'book_name_id'=>$book_id_old,
            'book_chapter_info_id'=>$chapterid
        ];
        $bookcontent = Bookcontent::where($where)
            ->field(['book_chapter_title','book_content','book_name_id','book_chapter_id','book_chapter_info_id',])
            ->find();
//        if (empty($bookcontent)){
//            $this->error('章节内容暂未更新');
//            $code=1;
//            $msg="章节内容暂未更新";
//            $mData = array("code"=>$code,"msg"=>$msg);
//            return $mData;
//        }
//        $bookcontent = $bookcontent->toArray();
//        if (count($bookcontent)>0){
//            $code=0;
//            $data=$bookcontent;
//            $msg="操作成功";
//            $mData = array("code"=>$code,"data"=> $data,"msg"=>$msg);
//        }else{
//            $code=1;
//            $msg="操作失败";
//            $mData = array("code"=>$code,"msg"=>$msg);
//        }
        return getReturnData($bookcontent);;
    }
}