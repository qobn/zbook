<?php
/**
 * 快读小说
 * 项目基于ThinkPHP5.1采集源基于笔趣阁
 * 写的不太好欢迎加入QQ群互相讨论：811389673
 * 点击链接加入群聊【小说快速搭建采集群】：https://jq.qq.com/?_wv=1027&k=5FADY8P
 * author:有点凉了
 */

namespace app\index\controller;

use app\index\model\Bookcontent;
use QL\QueryList;
use QL\Ext\CurlMulti;
use think\App;
use think\Controller;
use app\index\model\BookChapter;
use app\index\model\Bookname;
use app\index\model\Bookkind;
use app\index\model\Bookauthor;
use think\Db;
use think\facade\Request;

class Index extends Controller
{
    private $urls = [];
    private $urlLists = [];
    private $ids = [];
    private $chapter = array();
    private $bookname = [];
    private $bookchapterall = array();
    private $insertBookIdSelf;
    private $ip ='112.17.38.142:3128';

    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:) </h1><p> ThinkPHP V5.1<br/><span style="font-size:30px">12载初心不改（2006-2018） - 你值得信赖的PHP框架</span></p></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=64890268" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="eab4b9f840753f8e7"></think>';
    }

    /**
     * 全本小说网采集
     */
    public function getbooklists()
    {
        //采集规则
        $rules = [
            //采集a标签的href属性
            'novel_head' => ['.novel_name', 'text'],
            'novel_head_author' => ['.novel_info', 'text'],
            'book_content' => ['.novel_list>ul>li', 'html']
        ];
        $html = file_get_contents('http://www.quanben.co/files/article/html/1/1/main.html');
        $ql = QueryList::html($html)
            ->rules($rules)
            ->encoding('UTF-8', 'GB2312')
            ->removeHead()
            ->query();
        $data = $ql->getData();
        $result = $data->all();
        foreach ($result as $key => $value) {
            if ($key != 'novel_head') {
                foreach ($value as $lastvalue) {
                    $lastvalue . substr_replace("\"", '', 1);
                    $urls[] = "http://www.quanben.co/files/article/html/1/1/" . $this->findNum($lastvalue) . ".html";
                    if (count($urls) >= 100) {
                        continue;
                    }
                }
            }
        }
//        dump($urls);
        $this->getbookinfo($urls);
    }

    public function getbookinfo($urls = [])
    {
        if (count($urls) <= 0) {
            return;
        }
        $urls = array_chunk($urls, 500)[0];
        set_time_limit(3600);
        //采集规则  单线程采集
//        $ruleinfo = [
//            //采集a标签的href属性
//            'book_content' => ['.novel_content', 'text']
//        ];
//        // 设置采集规则
//        foreach($urls as $url){
//            $html = file_get_contents($url);
//            // 每条链接都会应用上面设置好的采集规则
//            $ql = QueryList::html($html)->rules($ruleinfo)->encoding('UTF-8', 'GB2312')->removeHead()->query();
//            $data = $ql->getData();
//            // 释放Document内存占用
//            $result = $data->all();
//            dump($result);
//            $ql->destruct();
//        }
        $ruleinfo = [
            //采集a标签的href属性
            'book_content' => ['.novel_content', 'text']
        ];
        $ql = QueryList::getInstance();
//        $ql->encoding('UTF-8', 'GB2312')->removeHead();
        $ql->use(CurlMulti::class);
        $ql->rules($ruleinfo)
            ->curlMulti($urls)
            ->success(function (QueryList $ql, CurlMulti $curl, $r) {
//                echo "Current url:{$r['info']['url']} \r\n";
                $data = $ql->encoding('UTF-8', 'GB2312')
                    ->removeHead()
                    ->query()
                    ->getData();
//                dump($data->all());
            })->start(/*[
                // 最大并发数，这个值可以运行中动态改变。
                'maxThread' => 10,
                // 触发curl错误或用户错误之前最大重试次数，超过次数$error指定的回调会被调用。
                'maxTry' => 3,
                // 全局CURLOPT_*
                'opt' => [
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_CONNECTTIMEOUT => 1,
                    CURLOPT_RETURNTRANSFER => true,
                ],
                // 缓存选项很容易被理解，缓存使用url来识别。如果使用缓存类库不会访问网络而是直接返回缓存。
                //'cache' => ['enable' => false, 'compress' => false, 'dir' => null, 'expire' =>86400, 'verifyPost' => false]
            ]*/);

    }


    /**
     * 从结果中提取目录id
     * @param string $str
     * @return string
     */
    public function findNum($str = '')
    {
        $str = trim($str);
        if (empty($str)) {
            return '';
        }
        $reg = '/(\d{6}(\.\d+)?)/is';//匹配数字的正则表达式
        preg_match_all($reg, $str, $result);
        if (is_array($result) && !empty($result) && !empty($result[1]) && !empty($result[1][0])) {
            return $result[1][0];
        }
        return '';
    }

    /**
     * 从采集结果中提取目录
     * @param $str
     * @param string $charset
     * @return bool|string
     */
    public function getChinese($str, $charset = 'utf8')
    {

        if ($charset == 'gb2312') {
            if (!preg_match_all("/^[" . chr(0xa1) . "-" . chr(0xff) . "]+/", $str, $match)) {
                return false;
            }
            return implode('', $match[0]);
        }
        if ($charset == 'utf8') {
            if (!preg_match_all("/[\x{4e00}-\x{9fa5}]+/u", $str, $match)) {
                return false;
            }
            return implode('', $match[0]);
        }
        return false;

    }

    /**
     * 生成随机id采集
     */
    public function makeids($rand = 0)
    {
        $rules = [
            //采集a标签的href属性
            'book_author' => ['#info>p:first', 'text'],
        ];
        $index = Request::param('index');
        $left = Request::param('left');
        $right = Request::param('right');
        if ($rand!=1){
            $rand = Request::param('rand');
        }
        if ($rand == 1) {
            $index = rand(1, 19);
            if (ISCESHI){
                $one = rand(900, 999);
                $two = rand(900, 999);
            }else{
                $one = rand(100, 999);
                $two = rand(100, 999);
            }
            if ($one > $two) {
                $left = $two;
                $right = $one;
            } else {
                $left = $one;
                $right = $two;
            }
        }
        set_time_limit(3600);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $urlmake = [];
        for ($i = $index; $i <= $index; $i++) {
            for ($j = $i . $left; $j <= $i . $right; $j++) {
                echo $i . '_' . $j;
                echo "<br>";
                if (ISCESHI) {
                    $url = LOCALURL . "/index.php/index/index/getbiqugelists/id/" . $i . '_' . $j;
                } else {
                    $url = SERVICE . "/index.php/index/index/getbiqugelists/id/" . $i . '_' . $j;
                }
                $urlmake [] = $url;
            }
        }
//        dump($urlmake);
        $ql = QueryList::getInstance();
        $ql->use(CurlMulti::class);
        $ql->rules($rules)
            ->curlMulti($urlmake)
            ->success(function (QueryList $ql, CurlMulti $curl, $r) {
                echo "Current url:{$r['info']['url']} \r\n";
                $data = $ql->encoding('UTF-8', 'GB2312')
                    ->removeHead()
                    ->query()->getData();
                $result = $data->all();
                if (!empty($result)) {
                    $this->getbiqugelists($r['info']['url']);
                }
                unset($urlmake);
                $ql->destruct();
            })->error(function ($errorInfo, CurlMulti $curl) {
                echo "Current url:{$errorInfo['info']['url']} \r\n";
                print_r($errorInfo['error']);
            })->start([
                // 最大并发数，这个值可以运行中动态改变。
                'maxThread' => 10,
                // 触发curl错误或用户错误之前最大重试次数，超过次数$error指定的回调会被调用。
                // 'maxTry' => 3,
                // 全局CURLOPT_
                'opt' => [
                    CURLOPT_TIMEOUT => 3600,
                    //                    CURLOPT_CONNECTTIMEOUT => 1,
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER=>false,
                    CURLOPT_REFERER => biqige,
                ],
                // 缓存选项很容易被理解，缓存使用url来识别。如果使用缓存类库不会访问网络而是直接返回缓存。
                //'cache' => ['enable' => false, 'compress' => false, 'dir' => null, 'expire' => 86400, 'verifyPost' => false]
            ]);
    }

    /**
     * 采集章节列表
     * http://www.biquge.com.tw
     * @param null $urlMake
     */
    public function getbiqugelists($urlMake = null)
    {
        set_time_limit(3600);   //执行时间无限
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '-1');
        $id = input('param.id');//接收id用于半自动采集

        $idss = [
            $id
        ];
        //采集规则
        $rules = [
            //采集a标签的href属性
            'book_kind' => ['.con_top', 'text'],
            'book_img' => ['#fmimg>img', 'src'],
            'book_name' => ['#info>h1', 'text'],
            'book_author' => ['#info>p:first', 'text'],
            'book_last_update_time' => ['#info>p:eq(2)', 'text'],
            'book_last_update_chapter' => ['#info>p:eq(3)', 'text'],
            'book_intro' => ["#intro>p:eq(1)", 'text'],
            'book_content' => ['.box_con>#list>dl>dd', 'html'],
            'book_content_href' => ['.box_con>#list>dl>dd>a', 'href'],
            'book_content_name' => ['.box_con>#list>dl>dd', 'text']
        ];
        //全自动采集
        if (empty($urlMake)) {
            foreach ($idss as $id) {
                $urllist [] = biqige . $id . '/';
            }
        } else {
            $urllist[] = $urlMake;
        }

        $ql = QueryList::getInstance();
        $ql->use(CurlMulti::class);
        $ql->rules($rules)
            ->curlMulti($urllist)
            ->success(function (QueryList $ql, CurlMulti $curl, $r) {
                echo "getbiqugelists Current url:{$r['info']['url']} \r\n";
                $data = $ql->encoding('UTF-8', 'GB2312')
                    ->removeHead()
                    ->query()->getData();
                $result = $data->all();
                dump($result);
                if (!empty($result)) {
                    $this->bookname [] = $result[0];
                    foreach ($result as $key => $value) {
                        $every = [];
                        $lastvalue = $value['book_content'];
                        $id = $value['book_content_href'];
                        var_dump($id);
                        $lastvalueIds = explode('/', $id);
                        var_dump($lastvalueIds);
                        var_dump($lastvalueIds[4]);
                        $every['book_chapter_id'] = str_replace(".html",'',$lastvalueIds[4]);
                        $every['book_chapter_name'] = $value['book_content_name'];
                        $this->chapter[] = $every;
                        $this->urlLists[] = biqige . $id;
                        $this->ids[] = $lastvalueIds[3];
                    }
                    $bookkinds = Bookkind::all();
                    $bookkinds = $bookkinds->toArray();
                    $booknamearray = $this->bookname;
                    $book_kindnametitle = [];
                    foreach ($booknamearray as $key => $value) {
                        $book_kindnametitle[] = $value['book_kind'];
                        $lastvalue = $value['book_content_href'];
                        $lastvalue = explode('/', $lastvalue);
                        $booknamearray[$key]['book_id_old'] = $lastvalue[3];
                        $booknamearray[$key]['book_intro'] = $value['book_intro'];
                    }
                    foreach ($bookkinds as $value) {

                        foreach ($book_kindnametitle as $key => $val) {
                            $index = stripos($book_kindnametitle[$key], $value['book_kind'], 1);
                            if ($index > 0) {
                                $booknamearray[$key]['book_kind'] = $value['id'];
                                break(2);
                            } else {
                                $booknamearray[$key]['book_kind'] = 19;
                            }
                        }
                    }
                    $insertAuthorIds = [];
                    $insertAuthorIds = $this->setAuthor($booknamearray);
                    if (!empty($this->ids)) {
                        $book_old_id = $this->ids[0];
                    }
                    $insertBookIds = [];
                    $insertBookIds = $this->setBookname($booknamearray, $insertAuthorIds, $bookname_push = [], $book_old_id);
                    if (!empty($insertBookIds)) {
                        $this->insertBookIdSelf = $insertBookIds[0];
                    } else {
                        $this->insertBookIdSelf = 0;
                    }
                    $this->getDataFromSqlCompare($booknamearray, $insertBookIds, $insertAuthorIds);
                    vardump($this->urlLists);
                    halt(1);
                    $this->getbiqugebookinfo($this->urlLists);
                    unset($insertBookIds);
                    unset($insertAuthorIds);
                    unset($booknamearray);
                    unset($this->ids);
                    unset($urllist);
                    unset($this->chapter);
                }
                $ql->destruct();
            })->error(function ($errorInfo, CurlMulti $curl) {
                echo "Current url:{$errorInfo['info']['url']} \r\n";
                print_r($errorInfo['error']);
            })->start([
                // 最大并发数，这个值可以运行中动态改变。
                'maxThread' => 10,
                // 触发curl错误或用户错误之前最大重试次数，超过次数$error指定的回调会被调用。
                'maxTry' => 3,
                // 全局CURLOPT_*
                'opt' => [
                    CURLOPT_TIMEOUT => 3600,
                    // CURLOPT_CONNECTTIMEOUT => 1,
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER=>false,
                    CURLOPT_PROXY => $this->ip,
                    CURLOPT_REFERER => biqige,
                ],
                // 缓存选项很容易被理解，缓存使用url来识别。如果使用缓存类库不会访问网络而是直接返回缓存。
                 'cache' => ['enable' => false, 'compress' => false, 'dir' => null, 'expire' => 86400, 'verifyPost' => false]
            ]);


    }

    /**
     * 采集详情
     * @param array $urls
     * @param bool $isself
     */
    public function getbiqugebookinfo($urls = [], $isself = true)
    {
        if (count($urls) <= 0) {
            return;
        }
        set_time_limit(3600);      //执行时间无限
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '-1');
        $ruleinfo = [
            'book_chapter_title' => ['.bookname>h1', 'text'],
            'book_content' => ['div#content', 'text']
        ];
        $ql = QueryList::getInstance();
        $ql->use(CurlMulti::class);
        $ql->rules($ruleinfo)
            ->curlMulti($urls)
            ->success(function (QueryList $ql, CurlMulti $curl, $r) {
                echo "Current url:{$r['info']['url']} \r\n" . "<br>";
                $url = $r['info']['url'];
                $url = explode('/', $url);
                $book_chapter_id = '/' . $url[3] . '/' . $url[4];
                $book_name_id = $url[3];
                $data = $ql->encoding('UTF-8', 'GB2312')
                    ->removeHead()
                    ->query()->getData();
                $result = $data->all();
                $book_chapter_ids = [];
                if (!empty($result)) {
                    foreach ($result as $key => $value) {
                        $result[$key]['book_name_id'] = $book_name_id;
                        $result[$key]['book_chapter_id'] = $book_chapter_id;
                        $book_chapter_ids = explode('/', $book_chapter_id);
                        $book_chapter_info_id = str_replace('.html', '', $book_chapter_ids[2]);
                        $result[$key]['book_chapter_info_id'] = $book_chapter_info_id;
                    }
                    $bookcontents = Bookcontent::where('book_name_id', $book_name_id)
                        ->field(['book_chapter_title', 'book_chapter_id'])
                        ->select();
                    $result = $this->setValueToKey($result, 'book_chapter_title', 'book_chapter_id');
                    $bookcontents = $this->setValueToKey($bookcontents, 'book_chapter_title', 'book_chapter_id');
                    foreach ($result as $key => $value) {
                        foreach ($bookcontents as $keyval => $val) {
                            if ($key == $keyval) {
                                unset($result[$key]);
                            }
                        }
                    }
                    $bookcontent = new Bookcontent();
                    $bookcontent->saveAll($result);
                }
                unset($result);
                unset($bookcontents);
                unset($this->urlLists);
                unset($urls);
                // 释放资源
                $ql->destruct();
            })->error(function ($errorInfo, CurlMulti $curl) {
                echo "Current url:{$errorInfo['info']['url']} \r\n";
                print_r($errorInfo['error']);
            })->start([
                // 最大并发数，这个值可以运行中动态改变。
                'maxThread' => 10,
                // 触发curl错误或用户错误之前最大重试次数，超过次数$error指定的回调会被调用。
                'maxTry' => 3,
                // 全局CURLOPT_*
                'opt' => [
                    CURLOPT_TIMEOUT => 3600,
                    //CURLOPT_CONNECTTIMEOUT => 1,
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER=>false,
                    CURLOPT_PROXY => $this->ip,
                    CURLOPT_REFERER => biqige,
                ],
                // 缓存选项很容易被理解，缓存使用url来识别。如果使用缓存类库不会访问网络而是直接返回缓存。
                'cache' => ['enable' => false, 'compress' => false, 'dir' => null, 'expire' => 86400, 'verifyPost' => false]
            ]);
    }

    /**
     * 根据数据库已经存储章节更新章节内容
     * @throws \think\exception\DbException
     */
    public function getBookContent()
    {
        set_time_limit(3600);
        $result = Db::query('SELECT DISTINCT bookchapter.book_chapter_id FROM bookchapter WHERE bookchapter.book_chapter_id not in(SELECT book_chapter_id FROM bookchaptercontent);');
        $urls = [];
        foreach ($result as $res) {
            $url = biqige . ($res['book_chapter_id']);
            $urls [] = $url;
        }
        $this->getbiqugebookinfo($urls, false);
    }

    /**
     * 重置索引
     * @param $res
     * @param $left
     * @param $right
     * @return array
     */
    public function setValueToKey($res, $left, $right)
    {
        if (count($res) <= 0) {
            return [];
        }
        $resReturn = [];
        foreach ($res as $key => $value) {
            $resReturn[$value[$left] . $value[$right]] = $value;
        }
        return $resReturn;
    }

    /**
     * 差异化插入章节信息
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getDataFromSqlCompare($booknamearray, $insertBookIds = [], $insertAuthorIds = [])
    {
        if (empty($booknamearray)) {
            return;
        }
        $sqldata = BookChapter::where('book_name_id', $booknamearray[0]['book_id_old'])
            ->field(['book_chapter_id', 'book_chapter_name'])
            ->select();
        $res = $sqldata->toArray();
        $chaptersmearge = $this->chapter;
        $book_chapter_ids = [];
        foreach ($chaptersmearge as $key => $value) {
            $chaptersmearge[$key]['book_name_id'] = $booknamearray[0]['book_id_old'];
            $chaptersmearge[$key]['book_author_id'] = $insertAuthorIds[0];
//            $book_chapter_ids = explode('/', $chaptersmearge[$key]['book_chapter_id']);
//            $book_chapter_info_id = str_replace('.html', '', $book_chapter_ids[2]);
            $book_chapter_info_id = $chaptersmearge[$key]['book_chapter_id'];
            $chaptersmearge[$key]['book_chapter_id'] = $book_chapter_info_id.".html";
            $chaptersmearge[$key]['book_chapter_info_id'] = $book_chapter_info_id;
        }
        $ressql_set_key_result = $this->setValueToKey($res, 'book_chapter_id', 'book_chapter_name');
        $netsql_set_key_result = $this->setValueToKey($chaptersmearge, 'book_chapter_id', 'book_chapter_name');
        foreach ($ressql_set_key_result as $key => $localval) {
            foreach ($netsql_set_key_result as $keynet => $netval) {
                if ($key == $keynet) {
                    unset($netsql_set_key_result[$keynet]);
                }
            }
        }
        $bookchapter = new BookChapter();
        $bookchapter->saveAll($netsql_set_key_result);
        unset($chaptersmearge);
        unset($netsql_set_key_result);
        unset($ressql_set_key_result);
        unset($res);
        unset($sqldata);
    }

    /**
     * 插入书名信息
     * @param $booknamearray
     * @param $bookname_push
     */
    public function setBookname($booknamearray, $insertAuthorIds, $bookname_push, $book_old_id)
    {
        $bookname_ids = [];
        foreach ($booknamearray as $key => $value) {
            $booknameself = Bookname::where('book_title', $value['book_name'])
                ->field(['book_author_id', 'book_title', 'id'])
                ->find();
            if (!empty($booknameself)) {
                $outside = $insertAuthorIds[$key] . $value['book_name'];
                $inside = $booknameself->book_author_id . $booknameself->book_title;
                if ($outside == $inside) {
                    $bookname_ids [] = $booknameself->id;
                }
            } else {
                $bookname_push['book_title'] = $value['book_name'];
                $bookname_push['book_kind_id'] = $value['book_kind'];
                $bookname_push['book_author_id'] = $insertAuthorIds[$key];
                $bookname_push['book_intro'] = $value['book_intro'];
                $bookname_push['book_img'] = $value['book_img'];
                $bookname_push['book_local_img'] = $value['book_img'];
//                $this->dlfile($bookname_push['book_img'], $bookname_push['book_local_img']);
                $bookname_push['book_id_old'] = $book_old_id;
                $bookname = Bookname::create($bookname_push);
                if ($bookname->id > 0) {
                    $bookname_ids [] = $book_old_id;
                }
            }
        }
        unset($bookname_push);
        return $bookname_ids;
    }

    function download($url, $path = 'images/')
    {
        $path = App::getInstance()->getRootPath() . 'public' . DIRECTORY_SEPARATOR . 'saveimgs';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_PROXY,$this->ip);
        $file = curl_exec($ch);
        curl_close($ch);
        $filename = pathinfo($url, PATHINFO_BASENAME);
        $resource = fopen($path . $filename, 'a');
        fwrite($resource, $file);
        fclose($resource);
    }

    /**
     * @param $booknamearray
     * @return string
     */
    public function setAuthor($booknamearray)
    {
        $insertAuthorIds = [];
        foreach ($booknamearray as $key => $value) {
            if (!empty($value['book_author'])){
                $book_author = explode("：", $value['book_author']);
                $book_autho = Bookauthor::where('author', $book_author[1])
                    ->field(['id', 'author'])
                    ->find();
            }
            if (!empty($book_autho)) {
                $insertAuthorIds [] = $book_autho->id;
            } else {
                $book_author = explode("：", $value['book_author']);
                $Bookauthor_array = [];
                $Bookauthor_array['author'] = $book_author[1];
                $Bookauthor_array['author_detail'] = $book_author[1];
                $bookSaveautho = Bookauthor::where('author',$Bookauthor_array['author'])->find();
                if (empty($bookSaveautho)){
                    $Bookauthor = Bookauthor::create($Bookauthor_array);
                    $insertAuthorIds [] = $Bookauthor->id;
                }else{
                    $insertAuthorIds [] = $bookSaveautho->id;
                }
            }
        }
        unset($Bookauthor_array);
        return $insertAuthorIds;
    }

    public function setChapterOther()
    {
        set_time_limit(3600);
        $index = Request::param('index');
        $size = Request::param('size');
        for ($i = $index; $i <= 143588; $i++) {//这里在需要的时候<=$size
            sleep(5);
            $bookchapter = BookChapter::get($i);
            if (!empty($bookchapter)) {
                if (strpos(($bookchapter->book_name_id), '_') > 0) {
                    continue;
                } else {
                    $book_chapter_ids = $bookchapter->book_chapter_id;
                    $book_chapter_ids = explode('/', $book_chapter_ids);
                    $book_chapter_id = $book_chapter_ids[1];
                    $bookchapter->book_name_id = $book_chapter_id;
                    $bookchapter->save();
                    unset($book_chapter_id);
                    unset($book_chapter_ids);
                }
                unset($bookchapter);
            }
        }
    }


    /**
     * 批量保存图书封面
     * @param $url
     * @param $from
     * @return mixed
     */
    function curl_file_get_contents($url, $from)
    {
        set_time_limit(0);
        //初始化curl会话
        $ch = curl_init();
        //设置一个cURL传输选项。
        curl_setopt($ch, CURLOPT_URL, $url);                    //目标
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        //curl_setopt($ch, CURLOPT_USERAGENT, _USERAGENT_);
        curl_setopt($ch, CURLOPT_REFERER, $from);            //伪造来路
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function dlfile($file_url, $savename)
    {
        $savename = str_replace('/', DIRECTORY_SEPARATOR, $savename);
        $save_to = App::getInstance()->getRootPath() . 'public' . DIRECTORY_SEPARATOR . 'saveimgs' . DIRECTORY_SEPARATOR . $savename;
        $filenames = explode(DIRECTORY_SEPARATOR, $save_to);
        $files = null;
        for ($i = 0; $i < count($filenames) - 1; $i++) {
            $files .= $filenames[$i] . DIRECTORY_SEPARATOR;
        }
        if (!file_exists($save_to)) {
            if (!file_exists($files)) {
                mkdir($files, 0777, true);
            }
            //$content = file_get_contents($file_url);
            $content = $this->curl_file_get_contents($file_url, biqige);
            file_put_contents($save_to, $content);
        }

    }

    /**
     * 将img_url存储到本地(先复制字段到book_local_img)
     */
    function copyImgSql(){
        $booknames = Bookname::where('book_local_img','')
            ->field(['id','book_img', 'book_local_img'])
            ->select();
        foreach ($booknames as $key=>$val){
            if (empty($val['book_local_img'])){
                $val['book_local_img'] = str_replace(biqige,'',$val['book_img']);
                $booknames[$key]['book_local_img'] = $val['book_local_img'];
                $this->dlfile($val['book_img'], $val['book_local_img']);
            }
        }
        $booknames = $booknames->toArray();
        $bookname = new Bookname();
        $bookname->saveAll($booknames);
    }
    /**
     * 如果 book_local_img 没有存在本地 那么执行这个批量保存到本地
     */
    function saveImg(){
        $booknames = Bookname::where('id','>','0')
            ->field(['id','book_img', 'book_local_img'])
            ->select();
        $dir = App::getInstance()->getRootPath() . 'public' . DIRECTORY_SEPARATOR . 'saveimgs';
        echo $dir;
        foreach ($booknames as $key=>$val){
            if (!file_exists($dir.$val['book_local_img'])){
                $this->dlfile($val['book_img'],$val['book_local_img']);
            }
        }
    }

    /**
     * 读取本地文件路径返回saveimgs 下边所有文件
     * @param $dir
     * @return array
     */
    function read_all_dir($dir)
    {
        $result = array();
        $results = array();
        $handle = opendir($dir);
        if ($handle) {
            while (( $file = readdir ( $handle ) ) !== false ) {
                if ( $file != '.' && $file != '..') {
                    $cur_path = $dir . DIRECTORY_SEPARATOR . $file;
                    if ( is_dir ( $cur_path ) ) {
                        $result['dir'][$cur_path] = $this->read_all_dir($cur_path);
                    }else {
                        $result['file'][] = $cur_path;
                        $results['file'][] = $cur_path;
                    }
                }
            }
            closedir($handle);
        }
        return $result;
    }

    /**
     * 获取代理ip
     */
    function getServerIp(){
        $baseUrl = 'https://www.kuaidaili.com/free/';
        //采集规则
        $rules = [
//            //采集a标签的href属性
            'ip' => ['td:eq(0)', 'text'],
            'port' => ['td:eq(1)', 'text'],
            'isanonymous' => ['td:eq(2)', 'text'],
            'isanonymous' => ['td:eq(2)', 'text'],
            'type' => ['td:eq(3)', 'text'],
            'position' => ['td:eq(4)', 'text'],
            'speed' => ['td:eq(5)', 'text'],
            'verificationTime' => ['td:eq(6)', 'text'],
        ];
        $html = file_get_contents($baseUrl);
        $ql = QueryList::html($html)
            ->rules($rules)
            ->encoding('UTF-8', 'UTF-8')
            ->removeHead()
            ->range("tbody>tr")//这个设置采集区域 ，就可以批量采集当前内容下对应索引的数据
            ->query();
        $data = $ql->getData();
        $result = $data->all();
        if (count($result)>0){
            foreach ($result as $key=>$value){
                if ($key==0){
                    $this->ip = $value['ip'].':'.$value['port'];
                }
            }
        }
        dump($this->ip);
        $this->makeids(1);
    }

    /**
     * 获取代理ip
     */
    function getServerIpContent(){
        $baseUrl = 'https://www.kuaidaili.com/free/';
        //采集规则
        $rules = [
//            //采集a标签的href属性
            'ip' => ['td:eq(0)', 'text'],
            'port' => ['td:eq(1)', 'text'],
            'isanonymous' => ['td:eq(2)', 'text'],
            'isanonymous' => ['td:eq(2)', 'text'],
            'type' => ['td:eq(3)', 'text'],
            'position' => ['td:eq(4)', 'text'],
            'speed' => ['td:eq(5)', 'text'],
            'verificationTime' => ['td:eq(6)', 'text'],
        ];
        $html = file_get_contents($baseUrl);
        $ql = QueryList::html($html)
            ->rules($rules)
            ->encoding('UTF-8', 'UTF-8')
            ->removeHead()
            ->range("tbody>tr")//这个设置采集区域 ，就可以批量采集当前内容下对应索引的数据
            ->query();
        $data = $ql->getData();
        $result = $data->all();
        if (count($result)>0){
            foreach ($result as $key=>$value){
                if ($key==0){
                    $this->ip = $value['ip'].':'.$value['port'];
                }
            }
        }
        dump($this->ip);
        $this->getBookContent();

    }
    /**
     * 开放代理ip接口
     */
    function getAgentServerIp(){
        $baseUrl = 'https://www.kuaidaili.com/free/';
        //采集规则
        $rules = [
//            //采集a标签的href属性
            'ip' => ['td:eq(0)', 'text'],
            'port' => ['td:eq(1)', 'text'],
            'isanonymous' => ['td:eq(2)', 'text'],
            'isanonymous' => ['td:eq(2)', 'text'],
            'type' => ['td:eq(3)', 'text'],
            'position' => ['td:eq(4)', 'text'],
            'speed' => ['td:eq(5)', 'text'],
            'verificationTime' => ['td:eq(6)', 'text'],
        ];
        $html = file_get_contents($baseUrl);
        $ql = QueryList::html($html)
            ->rules($rules)
            ->encoding('UTF-8', 'UTF-8')
            ->removeHead()
            ->range("tbody>tr")//这个设置采集区域 ，就可以批量采集当前内容下对应索引的数据
            ->query();
        $data = $ql->getData();
        $result = $data->all();
//        if (count($result)>0){
//            foreach ($result as $key=>$value){
//                if ($key==0){
//                    $this->ip = $value['ip'].':'.$value['port'];
//                }
//            }
//        }
        $res = array();
        if (count($result)>0){
            $res = ['code'=>0,'data'=>$result,'msg'=>'操作成功'];
        }else{
            $res = ['code'=>1,'data'=>$result,'msg'=>'操作失败'];
        }
        return json($res);
    }

    /**
     * set bookchapter book_chapter_info_id
     * @throws \Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function updateBookChapterInfoId(){
        $bookchapters = BookChapter::where('book_chapter_info_id',0)
            ->field(['id','book_chapter_id'])
            ->select();

        foreach ($bookchapters as $key=>$val){
            $book_chapter_ids = explode('/', $bookchapters[$key]['book_chapter_id']);
            $book_chapter_info_id = str_replace('.html', '', $book_chapter_ids[2]);
            $bookchapters[$key]['book_chapter_info_id'] = $book_chapter_info_id;
        }
        $bookchapterArray = $bookchapters->toArray();
        $BookChapter =new BookChapter();
        $BookChapter->saveAll($bookchapterArray);
    }

    /**
     * set bookchaptercontent book_chapter_info_id
     * @throws \Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function updateBookChapterContentInfoId(){
        $bookcontents = Bookcontent::where('book_chapter_info_id',0)
            ->field(['id','book_chapter_id'])
            ->select();
        foreach ($bookcontents as $key=>$val){
            $book_chapter_ids = explode('/', $bookcontents[$key]['book_chapter_id']);
            $book_chapter_info_id = str_replace('.html', '', $book_chapter_ids[2]);
            $bookcontents[$key]['book_chapter_info_id'] = $book_chapter_info_id;
        }
        $bookContentArray = $bookcontents->toArray();
        $BookContent =new Bookcontent();
        $BookContent->saveAll($bookContentArray);
    }
    /**
     * 测试路径
     */
    function test()
    {
        echo "RootPath" . App::getInstance()->getRootPath() . 'public' . DIRECTORY_SEPARATOR . 'saveimgs';
        echo "<br>";
    }
    function testAuthor(){
        $book_autho = Bookauthor::where('author', '神见')
            ->field(['id', 'author'])
            ->find();
        if (!empty($book_autho)){
            dump($book_autho['id']);
        }
    }

    function textUrl(){

        $url = "https://www.biquge5200.cc/75_75584/161904992.html";
        $url = explode('/', $url);
        $book_chapter_id = '/' . $url[3] . '/' . $url[4];
        $book_name_id0 = $url[0];
        $book_name_id1 = $url[1];
        $book_name_id2 = $url[2];
        $book_name_id3 = $url[3];
        $book_name_id4 = $url[4];
        var_dump($book_name_id0);
        var_dump($book_name_id1);
        var_dump($book_name_id2);
        var_dump($book_name_id3);
        var_dump($book_name_id4);
    }
}


