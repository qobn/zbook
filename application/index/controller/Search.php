<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/5 0005
 * Time: 上午 10:31
 */

namespace app\index\controller;


use app\index\model\Bookname;
use think\Controller;
use think\Db;
use think\facade\Request;

class Search extends Controller
{
    public function search()
    {

        $searchkey = Request::param('searchkey');
        $searchkey = trim($searchkey);

        $sql = "select * from bookname WHERE  book_title like ";
        $sql .= " '%{$searchkey}%' ";
        $booknames = Db::query($sql);
        foreach ($booknames as $key => $value) {
            $booknames[$key]['book_kind'] = $this->getKindName($value['book_kind_id']);
            $sqlauth = "select * from bookauthor WHERE id = ";
            $sqlauth .= $value['book_author_id'];
            $bookauthors = Db::query($sqlauth);
            $booknames[$key]['author'] = $bookauthors[0]['author'];

            $sqlbookchapter = "select * from bookchaptercontent WHERE book_name_id = ";
            $sqlbookchapter .= " '{$value['book_id_old']}' ";
            $sqlbookchapter .= ' ORDER BY  book_chapter_info_id  DESC LIMIT 1 ';
            $bookchapterlimit = Db::query($sqlbookchapter);
            $booknames[$key]['book_chapter_title'] = $bookchapterlimit[0]['book_chapter_title'];
            $booknames[$key]['book_chapter_info_id'] = $bookchapterlimit[0]['book_chapter_info_id'];
        }
        $this->view->assign('booknames', $booknames);
        $this->view->assign('searchkey', $searchkey);
        return $this->view->fetch();
    }

    public function getKindName($kind_id)
    {
        switch ($kind_id) {
            case 0:
                return '未知';
                break;
            case 1:
                return '玄幻';
                break;
            case 2:
                return '奇幻';
                break;
            case 3:
                return '都市';
                break;
            case 4:
                return '言情';
                break;
            case 5:
                return '武侠';
                break;
            case 6:
                return '仙侠';
                break;
            case 7:
                return '历史';
                break;
            case 8:
                return '军事';
                break;
            case 9:
                return '网游';
                break;
            case 10:
                return '竞技';
                break;
            case 11:
                return '科幻';
                break;
            case 12:
                return '灵异';
                break;
            case 13:
                return '同人';
                break;
            case 14:
                return '漫画';
                break;
            case 15:
                return '修真';
                break;
            case 16:
                return '修仙';
                break;
        }
    }

    //章节数据库规则导出
    public function SearchKey()
    {
        $searchkey = Request::param('searchkey');
        $searchkey = trim($searchkey);
        $sql = "select * from bookname WHERE  book_title like ";
        $sql .= " '%{$searchkey}%' ";
        $booknames = Db::query($sql);
        if (count($booknames) > 1) {
            return;
        }
        $book_name_id = $booknames[0]['book_id_old'];
        $sqlContent = "select * from bookchaptercontent WHERE book_name_id ='{$book_name_id}'";
        $sqlContent .= " order by book_chapter_info_id asc";
        $bookContents = Db::query($sqlContent);

        foreach ($bookContents as $key => $value) {
            $bookContents[$key]['book_chapter_title'] = "###" . $bookContents[$key]['book_chapter_title'];
        }
        $filename = "d:/" . $booknames[0]['book_title'] . '.txt';
        $encode = mb_detect_encoding($filename, array("ASCII", "UTF8", "GB2312", "GBK", "BIG5"));
        $fileName = iconv($encode, 'UTF8', $filename);
        $myfile = fopen($fileName, 'w') or die("Unable to open file!");
        foreach ($bookContents as $key => $value) {
            fwrite($myfile, $value['book_chapter_title'] . "\r\n");
            fwrite($myfile, $value['book_content'] . "\r\n");
        }
        fclose($myfile);
    }


    //章节数据库规则导出
    public function Savetolocal()
    {
        $searchid = Request::param('searchid');
        $searchid = trim($searchid);
        $sql = "select * from bookname WHERE  book_id_old = ";
        $sql .= " '{$searchid}' ";
        $booknames = Db::query($sql);
        if (count($booknames) > 1) {
            return;
        }
        $book_name_id = $booknames[0]['book_id_old'];
        $sqlContent = "select * from bookchaptercontent WHERE book_name_id ='{$book_name_id}'";
        $sqlContent .= " order by book_chapter_info_id asc";
        $bookContents = Db::query($sqlContent);

        foreach ($bookContents as $key => $value) {
            $bookContents[$key]['book_chapter_title'] = "###" . $bookContents[$key]['book_chapter_title'];
        }
        $filename = $_SERVER['DOCUMENT_ROOT'] . "/savebooks/" . $booknames[0]['book_title'] . '.txt';
        $encode = mb_detect_encoding($filename, array("ASCII", "UTF8", "GB2312", "GBK", "BIG5"));
        $fileName = iconv($encode, 'GBK', $filename);
//        $fileName = iconv('UTF-8', 'UTF-8', $filename);
        $myfile = fopen($fileName, 'w') or die("Unable to open file!");
        foreach ($bookContents as $key => $value) {
            fwrite($myfile, $value['book_chapter_title'] . "\r\n");
            fwrite($myfile, $value['book_content'] . "\r\n");
        }
        header($filename);
        fclose($myfile);
    }

    public function downBook($bookname = "斗破苍穹")
    {
        $bookname = Request::param("bookname");
        $file = downBookUrl() . $bookname . '.txt';
        halt($file);
        $filename = $file; //文件路径
        $mime = 'application/force-download';
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: ' . $mime);
        header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: close');
        readfile($filename);
        exit();
    }

    public function getaa()
    {
        header("Content-Type: application/octet-stream");

        $quan = Request::param("quan");
        $ua = $_SERVER["HTTP_USER_AGENT"];
        $filename = "100yuan.txt";
        $encoded_filename = urlencode($filename);
        $encoded_filename = str_replace("+", "%20", $encoded_filename);

        if (preg_match("/MSIE/", $_SERVER['HTTP_USER_AGENT'])) {
            header('Content-Disposition:  attachment; filename="' . $encoded_filename . '"');
        } elseif (preg_match("/Firefox/", $_SERVER['HTTP_USER_AGENT'])) {
            // header('Content-Disposition: attachment; filename*="utf8' .  $filename . '"');
            header('Content-Disposition: attachment; filename*="' . $filename . '"');
        } else {
            header('Content-Disposition: attachment; filename="' . $filename . '"');
        }

        echo '您已领取100元现金券。现金券编码是：' . $quan;
    }

    public function getbb()
    {
        $bookname = Request::param('bookname');
        $filename = downBookUrl() . $bookname . '.txt';
        $fileinfo = pathinfo($filename);
        header('Content-type: application/x-' . $fileinfo['extension']);
        header('Content-Disposition: attachment; filename=' . $fileinfo['basename']);
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit();
    }

    public function getcc()
    {
        $bookname = Request::param('bookname');
        $filename = downBookUrl() . $bookname . '.txt';
        $fileinfo = pathinfo($filename);

        $fCont = file_get_contents($filename);
        $fCont = mb_convert_encoding($fCont, 'utf-8', 'gbk');
        echo (strlen($fCont) / 1024) . "k";
        dump($filename);
        dump($fileinfo);
        halt(1);
        header('Content-type: application/x-' . $fileinfo['extension']);
        header('Content-Disposition: attachment; filename=' . $fileinfo['basename']);
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit();
    }

    public function getdd()
    {
        $bookname = Request::param('bookname');
        $file = $_SERVER['DOCUMENT_ROOT'] . "/savebooks/" . $bookname . '.txt';
        $fileinfo = pathinfo($file);
        $mode = 'r';
        header('Content-Type:' . $fileinfo['extension']);
        header('Content-Length:' . filesize($file));
        header('Content-Disposition:Attachment;filename=' . basename($file));
        $handle = fopen($file, $mode);
        while (!feof($handle)) {
            echo fgets($handle, 1024);
        }
        fclose($handle);
    }
}