<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/15 0015
 * Time: 上午 8:24
 */

namespace app\index\controller;


use app\index\model\Bookauthor;
use app\index\model\BookChapter;
use app\index\model\Bookcontent;
use app\index\model\Bookkind;
use app\index\model\Bookname;
use think\Controller;
use think\Db;
use think\facade\Request;

class Main extends Controller
{
    public function main()
    {
        $id = input('get.id');
//        $result = Bookname::all();

//        $sqlEvery25 ='SELECT * FROM bookname AS a WHERE (SELECT  COUNT(*) FROM  bookname AS b WHERE b.book_kind_id = a.book_kind_id AND b.id >= a.id) <= 12 ORDER BY a.book_kind_id ASC , a.id DESC';
//        $result = Db::query($sqlEvery25);
        $xuanhuans = Bookname::where('book_kind_id',1)
            ->limit(12)
            ->select();
        $xuanhuans = $xuanhuans->toArray();
        $dushus = Bookname::where('book_kind_id',3)
            ->limit(12)
            ->select();
        $dushus = $dushus->toArray();
        $xiuzhens = Bookname::where('book_kind_id',15)
            ->limit(12)
            ->select();
        $xiuzhens = $xiuzhens->toArray();
        $lishis = Bookname::where('book_kind_id',7)
            ->limit(12)
            ->select();
        $lishis = $lishis->toArray();
        $wangyous = Bookname::where('book_kind_id',9)
            ->limit(12)
            ->select();
        $wangyous = $wangyous->toArray();
        $kehuans = Bookname::where('book_kind_id',11)
            ->limit(12)
            ->select();
        $kehuans = $kehuans->toArray();
        $result = array_merge($xuanhuans,$dushus,$xiuzhens,$lishis,$wangyous,$kehuans);
        unset($xuanhuans);
        unset($dushus);
        unset($xiuzhens);
        unset($lishis);
        unset($wangyous);
        unset($kehuans);
        $bookauthors = Bookauthor::all();
        $bookauthors = $bookauthors->toArray();
        $bookkinds = Bookkind::all();
        $bookkinds = $bookkinds->toArray();
        foreach ($result as $key => $res) {
            foreach ($bookauthors as $keyInfo => $bookauthor) {
                if ($res['book_author_id'] == $bookauthor['id']) {
                    if (empty($bookauthor['author'])){
                        $result[$key]['author'] = "出错了";
                    }else{
                        $result[$key]['author'] = $bookauthor['author'];
                    }

                }
                foreach ($bookkinds as $bookkind) {
                    if ($result[$key]['book_kind_id'] == $bookkind['id']) {
                        $result[$key]['book_kind_name'] = $bookkind['book_kind'];
                    }
                    if ($result[$key]['book_kind_id'] == 0) {
                        $result[$key]['book_kind_name'] = '未知';
                    }
                }
            }
        }
        $xuanhuanResults = [];
        $xiuzhenResults = [];
        $dushiResults = [];
        $lishiResults = [];
        $wangyouResults = [];
        $kehuanResults = [];
        foreach ($result as $key => $value) {
            if ($value['book_kind_id'] == 1) {
                $xuanhuanResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 15) {
                $xiuzhenResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 3) {
                $dushiResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 7) {
                $lishiResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 9) {
                $wangyouResults [] = $result[$key];
            } elseif ($value['book_kind_id'] == 11) {
                $kehuanResults [] = $result[$key];
            }
        }

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('id','>','0')
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            foreach ($bookauthors as $keyInfo => $bookauthor) {
                if ($value['book_author_id'] == $bookauthor['id']) {
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "出错了";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }
                }
            }
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }
        }
//        dump($booknamesUpdates);
        $booknamesCreates = Bookname::where('id','>','0')
            ->order('create_time desc')
            ->limit(25)
            ->select();
        $booknamesCreates = $booknamesCreates->toArray();
        foreach ($booknamesCreates as $key=>$value){
            foreach ($bookauthors as $keyInfo => $bookauthor) {
                if ($value['book_author_id'] == $bookauthor['id']) {
                    if (empty($bookauthor['author'])){
                        $booknamesCreates[$key]['author'] = "出错了";
                    }else{
                        $booknamesCreates[$key]['author'] = $bookauthor['author'];
                    }
                }
            }
            foreach ($bookkinds as $bookkind) {
                if ($booknamesCreates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesCreates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesCreates[$key]['book_kind_id'] == 0) {
                    $booknamesCreates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $this->assign('result', $result);
        $this->assign('xuanhuanResults', $xuanhuanResults);
        $this->assign('xiuzhenResults', $xiuzhenResults);
        $this->assign('dushiResults', $dushiResults);
        $this->assign('lishiResults', $lishiResults);
        $this->assign('wangyouResults', $wangyouResults);
        $this->assign('kehuanResults', $kehuanResults);
        $this->assign('booknamesCreates', $booknamesCreates);
        $this->assign('booknamesUpdates', $booknamesUpdates);
        return $this->fetch('main');    }

    /**
     * 玄幻
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function xuanhuan()
    {
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('book_kind_id',$id)
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }

            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }

                }
            }
        }
        $this->view->assign('booknames',$booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function xiuzhen()
    {
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('book_kind_id',$id)
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }
            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }
                }
            }
        }
        $this->view->assign('booknames',$booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function dushi()
    {
//        $id = input('get.id');
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);
        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('book_kind_id',$id)
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }
            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }

                }
            }
        }
        $this->view->assign('booknames',$booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function lishi()
    {
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('book_kind_id',$id)
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }

            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }

                }
            }
        }
        $this->view->assign('booknames',$booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function wangyou()
    {
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('book_kind_id',$id)
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }

            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }

                }
            }
        }
        $this->view->assign('booknames',$booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function kehuan()
    {
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('book_kind_id',$id)
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }

            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }

                }
            }
        }
        $this->view->assign('booknames',$booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function kongbu()
    {
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('book_kind_id',$id)
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }
            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }

                }
            }
        }
        $this->view->assign('booknames',$booknames);
//        halt($booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function quanben()
    {
        $id=Request::param('id');
        list($bookkinds, $booknames, $bookauthors, $key, $bookauthor) = $this->setTop6($id);

        //查询最近更新的25本
        $booknamesUpdates = Bookname::where('id','>','0')
            ->order('update_time desc')
            ->limit(25)
            ->select();
        $booknamesUpdates = $booknamesUpdates->toArray();
        $booknamesUpdate_ids = [];
        foreach ($booknamesUpdates as $key=>$value){
            $booknamesUpdate_ids [] = $value['book_id_old'];
            $booknamesUpdate_authorIds [] = $value['book_author_id'];
            foreach ($bookkinds as $bookkind) {
                if ($booknamesUpdates[$key]['book_kind_id'] == $bookkind['id']) {
                    $booknamesUpdates[$key]['book_kind_name'] = $bookkind['book_kind'];
                }
                if ($booknamesUpdates[$key]['book_kind_id'] == 0) {
                    $booknamesUpdates[$key]['book_kind_name'] = '未知';
                }
            }
        }
        $bookauthors25 = Bookauthor::where(['id' => $booknamesUpdate_authorIds])
            ->select();
        $bookauthors25 = $bookauthors25->toArray();
        foreach ($booknamesUpdate_ids as $key=>$value){
            $sql = "SELECT * from bookchaptercontent WHERE book_chapter_id like ";
            $sql.=" '/{$value}%' ";
            $sql.=" ORDER BY book_chapter_info_id desc";
            $sql.=" limit 1";
            $bookchaptercontent = Db::query($sql);
            if (empty($bookchaptercontent)){
                $booknamesUpdates[$key]['book_chapter_title'] = "暂未更新";
                $booknamesUpdates[$key]['book_chapter_id'] = "#";
                $booknamesUpdates[$key]['update_time'] = time();
                $booknamesUpdates[$key]['book_chapter_info_id'] = 0;
            }else{
                $booknamesUpdates[$key]['book_chapter_title'] = $bookchaptercontent[0]['book_chapter_title'];
                $booknamesUpdates[$key]['update_time'] = $bookchaptercontent[0]['update_time'];
                $book_chapter_id =  str_replace('/','-',$bookchaptercontent[0]['book_chapter_id']);
                $booknamesUpdates[$key]['book_chapter_id'] = $book_chapter_id;
                $booknamesUpdates[$key]['book_chapter_info_id'] = $bookchaptercontent[0]['book_chapter_info_id'];
            }

            foreach ($bookauthors25 as $bookauthor){
                if ($booknamesUpdates[$key]['book_author_id'] == $bookauthor['id']){
                    if (empty($bookauthor['author'])){
                        $booknamesUpdates[$key]['author'] = "未知";
                    }else{
                        $booknamesUpdates[$key]['author'] = $bookauthor['author'];
                    }

                }
            }
        }
        $this->view->assign('booknames',$booknames);
        $this->view->assign('booknamesUpdates',$booknamesUpdates);
        return $this->fetch('bookother');
    }

    public function booklists()
    {
        $id = Request::param('id');

        $bookname = Bookname::where('book_id_old', $id)
            ->find();
        if (empty($bookname)){
            $this->error("出错了");
        }
        $kind_name = $this->getKindName($bookname->book_kind_id);

        $bookaurhor = Bookauthor::where('id',$bookname->book_author_id)
            ->find();

        $book_aurhor = $bookaurhor->author;

        $sql = "SELECT * from bookchapter WHERE book_chapter_id like ";
        $sql.=" '/{$bookname->book_id_old}%' ";
        $sql.=" ORDER BY book_chapter_info_id asc";
        $bookchapters = Db::query($sql);
        if (empty($bookchapters)){
            $this->error('章节为空');
        }
//        foreach ($bookchapters as $key=>$bookchapter){
//            $bookchapters[$key]['book_chapter_id'] =str_replace('/','-',$bookchapter['book_chapter_id']);
//            $bookids = explode('/',$bookchapter['book_chapter_id']);
//            $book_chapter_id_detail = str_replace(".html","",$bookids[2]);
//            $bookchapters[$key]['book_chapter_id_detail'] = $book_chapter_id_detail;
//        }
//        $bookchapters = $this->arraySort($bookchapters,'book_chapter_info_id',SORT_ASC);
        $this->view->assign('kind_name',$kind_name);
        $this->view->assign('book_aurhor',$book_aurhor);
        $this->view->assign('bookchapters',$bookchapters);
        $this->view->assign('bookname',$bookname);
        $this->view->assign('bookid',$id);
        return $this->view->fetch();
    }

    public function bookInfo(){
        $book_id_old = Request::param('id');
        $chapterid = Request::param('chapterid');

        $bookname = Bookname::where('book_id_old',$book_id_old)
            ->field(['book_title','book_kind_id','book_id_old'])
            ->find();
        $book_title = $bookname->book_title;
        $kind_name = $this->getKindName($bookname->book_kind_id);
//        $book_chapter_id = str_replace('-','/',$id).'.html';
        $where = [
            'book_name_id'=>$book_id_old,
            'book_chapter_info_id'=>$chapterid
        ];
        $bookcontent = Bookcontent::where($where)->find();
        if (empty($bookcontent)){
            $this->error('章节内容暂未更新');
        }
        $bookcontent = $bookcontent->toArray();
//        $book_chapter_ids = explode('/',$book_chapter_id);
//        $book_chapter_id = str_replace('.html','',$book_chapter_ids[2]);

        $sqlleft = "SELECT * from bookchaptercontent WHERE book_name_id =  ";
        $sqlleft.=" '{$bookname->book_id_old}' ";
        $sqlleft .= " AND book_chapter_info_id<'{$chapterid}' ";
        $sqlleft.=" ORDER BY book_chapter_info_id desc ";
        $sqlleft.=" limit 1 ";
        $bookchaptercontentleft = Db::query($sqlleft);

        $sqlright = "SELECT * from bookchaptercontent WHERE book_name_id = ";
        $sqlright.=" '{$bookname->book_id_old}' ";
        $sqlright .= " AND book_chapter_info_id>'{$chapterid}' ";
        $sqlright.=" ORDER BY book_chapter_info_id ASC";
        $sqlright.=" limit 1 ";
        $bookchaptercontentright = Db::query($sqlright);
        if (!empty($bookchaptercontentleft)){
            $book_chapter_id_left = $bookchaptercontentleft[0]['book_chapter_info_id'];
        }
        if (!empty($bookchaptercontentright)){
            $book_chapter_id_right = $bookchaptercontentright[0]['book_chapter_info_id'];
        }
        if (empty($bookchaptercontentleft)){
            $book_chapter_id_left =$chapterid;
        }elseif (empty($bookchaptercontentright)){
            $book_chapter_id_right = $chapterid;
        }
        $this->view->assign('book_chapter_id_left',$book_chapter_id_left);
        $this->view->assign('book_chapter_id_right',$book_chapter_id_right);
        $this->view->assign('book_title',$book_title);
        $this->view->assign('kind_name',$kind_name);
        $this->view->assign('bookcontent',$bookcontent);
        $this->view->assign('book_id_old',$book_id_old);
        return $this->view->fetch();
    }

    public function getKindName($kind_id)
    {
        switch ($kind_id) {
            case 0:
                return '未知';
                break;
            case 1:
                return '玄幻';
                break;
            case 2:
                return '奇幻';
                break;
            case 3:
                return '都市';
                break;
            case 4:
                return '言情';
                break;
            case 5:
                return '武侠';
                break;
            case 6:
                return '仙侠';
                break;
            case 7:
                return '历史';
                break;
            case 8:
                return '军事';
                break;
            case 9:
                return '网游';
                break;
            case 10:
                return '竞技';
                break;
            case 11:
                return '科幻';
                break;
            case 12:
                return '灵异';
                break;
            case 13:
                return '同人';
                break;
            case 14:
                return '漫画';
                break;
            case 15:
                return '修真';
                break;
            case 16:
                return '修仙';
                break;
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function setTop6($kind =1)
    {
        $bookkinds = Bookkind::all();
        $bookkinds = $bookkinds->toArray();

        $booknames = Bookname::where('book_kind_id', $kind)
            ->order('update_time desc')
            ->limit(6)
            ->select();
        $booknames = $booknames->toArray();
        $bookauthorIds = [];
        foreach ($booknames as $bookname) {
            $bookauthorIds [] = $bookname['book_author_id'];
        }
        $bookauthors = Bookauthor::where(['id' => $bookauthorIds])
            ->select();
        $bookauthors = $bookauthors->toArray();
        foreach ($booknames as $key => $bookname) {
            foreach ($bookauthors as $bookauthor) {
                if ($bookname['book_author_id'] == $bookauthor['id']) {
                    if (empty($bookauthor['author'])) {
                        $booknames[$key]['author'] = "未知";
                    } else {
                        $booknames[$key]['author'] = $bookauthor['author'];
                    }
                }
            }
            $booknames[$key]['book_kind_name'] = $this->getKindName($bookname['book_kind_id']);;
        }
        return array($bookkinds, $booknames, $bookauthors, $key, $bookauthor);
    }

    /**
     * 二维数组根据某个字段排序
     * @param array $array 要排序的数组
     * @param string $keys   要排序的键字段
     * @param string $sort  排序类型  SORT_ASC     SORT_DESC
     * @return array 排序后的数组
     */
    function arraySort($array, $keys, $sort = 'SORT_ASC') {
        $keysValue = [];
        foreach ($array as $k => $v) {
            $keysValue[$k] = $v[$keys];
        }
        array_multisort($keysValue, $sort, $array);
        return $array;
    }
    public function mainIndex(){
        return $this->view->fetch('index');
    }
}