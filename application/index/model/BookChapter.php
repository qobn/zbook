<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9 0009
 * Time: 下午 4:20
 */
namespace app\index\model;
use think\Model;
use think\model\concern\SoftDelete;
class BookChapter extends Model
{
    use SoftDelete;
    protected $table = 'bookchapter';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
}