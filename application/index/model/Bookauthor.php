<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/10 0010
 * Time: 上午 11:13
 */

namespace app\index\model;


use think\Model;
use think\model\concern\SoftDelete;
class Bookauthor extends Model
{
    use SoftDelete;
    protected $table = 'bookauthor';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
}