<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/10 0010
 * Time: 下午 4:36
 */

namespace app\index\model;


use think\Model;
use think\model\concern\SoftDelete;
class Bookcontent extends Model
{
    use SoftDelete;
    protected $table = 'bookchaptercontent';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
}