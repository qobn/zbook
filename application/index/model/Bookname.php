<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9 0009
 * Time: 下午 6:54
 */

namespace app\index\model;


use think\Model;
use think\model\concern\SoftDelete;

class Bookname extends Model
{
    use SoftDelete;
    protected $table = 'bookname';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
}