/*
Navicat MySQL Data Transfer

Source Server         : 47.98.179.79
Source Server Version : 50560
Source Host           : 47.98.179.79:3306
Source Database       : sql47_98_179_79

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2020-11-16 12:57:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bookkind
-- ----------------------------
DROP TABLE IF EXISTS `bookkind`;
CREATE TABLE `bookkind` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `book_kind` varchar(255) NOT NULL COMMENT '小说类别',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bookkind
-- ----------------------------
INSERT INTO `bookkind` VALUES ('1', '玄幻');
INSERT INTO `bookkind` VALUES ('2', '奇幻');
INSERT INTO `bookkind` VALUES ('3', '都市');
INSERT INTO `bookkind` VALUES ('4', '言情');
INSERT INTO `bookkind` VALUES ('5', '武侠');
INSERT INTO `bookkind` VALUES ('6', '仙侠');
INSERT INTO `bookkind` VALUES ('7', '历史');
INSERT INTO `bookkind` VALUES ('8', '军事');
INSERT INTO `bookkind` VALUES ('9', '网游');
INSERT INTO `bookkind` VALUES ('10', '竞技');
INSERT INTO `bookkind` VALUES ('11', '科幻');
INSERT INTO `bookkind` VALUES ('12', '灵异');
INSERT INTO `bookkind` VALUES ('13', '同人');
INSERT INTO `bookkind` VALUES ('14', '漫画');
INSERT INTO `bookkind` VALUES ('15', '修真');
INSERT INTO `bookkind` VALUES ('16', '修仙');
INSERT INTO `bookkind` VALUES ('17', '恐怖');
INSERT INTO `bookkind` VALUES ('18', '全本');
INSERT INTO `bookkind` VALUES ('19', '其他');
