<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

Route::get('hello/:name', 'admin/hello');

Route::get('v1/bookmain', 'phoneapi/v1.bookmain/bookmain');//多级路由设置 在控制器下增加目录 需要路由到对应的
Route::post('v1/bookmain', 'phoneapi/v1.bookmain/bookmain');//多级路由设置 在控制器下增加目录 需要路由到对应的

Route::get('v1/booklist', 'phoneapi/v1.bookmain/booklist');//多级路由设置 在控制器下增加目录 需要路由到对应的
Route::post('v1/booklist', 'phoneapi/v1.bookmain/booklist');//多级路由设置 在控制器下增加目录 需要路由到对应的

Route::get('v1/bookinfo', 'phoneapi/v1.bookmain/bookinfo');//多级路由设置 在控制器下增加目录 需要路由到对应的
Route::post('v1/bookinfo', 'phoneapi/v1.bookmain/bookinfo');//多级路由设置 在控制器下增加目录 需要路由到对应的


Route::get('v1/booktype', 'phoneapi/v1.booktype/queryType');//多级路由设置 在控制器下增加目录 需要路由到对应的
Route::post('v1/booktype', 'phoneapi/v1.booktype/queryType');//多级路由设置 在控制器下增加目录 需要路由到对应的
/**
 * 路由地址中支持多级控制器， 使用下面的方式进行设置：
 * Route::get('blog/:id','index/group.blog/read');
 * 表示路由到下面的控制器类，
 * index/controller/group/Blog
 */
return [

];
